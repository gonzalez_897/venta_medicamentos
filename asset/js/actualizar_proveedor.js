$('#actualizar_proveedor').click(function() {


	var id_proveedor = $('#id_proveedor').val(),
	primer_nombre = $('#primer_nombre').val(),
	primer_apellido = $('#primer_apellido').val(),
	correo = $('#correo').val(),
	edad = $('#edad').val(),
	telefono = $('#telefono').val(),
	empresa = $('#empresa').val();
	
	$.ajax({
		dataType: 'json',                         
		url: baseurl+'control_proveedores/actualizar_proveedores',
		type: 'post',
		dataType: 'json',
		data:{

			id_proveedor,primer_nombre, primer_apellido, correo, edad, telefono, empresa
		},

		dataType: 'json',
		beforesend: function(){
			
		},
		success: function(data){
			if (data.success === 1) {
					alert("El proveedor ha sido registrado");
					document.location.href=baseurl+'control_proveedores/proveedor';
				}else{
					alert("No se pudo registrar al proveedor");
				}
			},
		error:function(e){

			alert("¡Algo Falló, Consulte al administrador!");
			console.log(e);
		}
	});
});