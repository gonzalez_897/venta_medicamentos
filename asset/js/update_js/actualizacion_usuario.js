$('#update_user').click(function () {
	var id_usuario = $('#idusuario').val(),
	nombre = $('#nombre').val(),
	apellido = $('#apellido').val(),
	edad = $('#edad').val(),
	sexo = $('#sexo').val(),
	correo = $('#correo').val(),
	usuario = $('#usuario').val(),
	contraseña = $('#contraseña').val();

	$.ajax({
		dataType:'json',
		url: baseurl+'control_de_usuarios/actualizar_usuarios',
		type: 'post',
		dataType: 'json',
		data: { id_usuario, nombre, apellido, edad, sexo, correo, usuario, contraseña },

		dataType:'json',

		success: function (data) {
			
			if (data.success === 1) {
				alert('Usuario Actualizado');
				document.location.href=baseurl+'control_de_usuarios/';
			}else{
				alert('Usuario no Actualizado');
			}
		},
		error: function (e) {
			alert('!Algo Fallo, Intentelo Mas Tarde');
			console.log(e);
		}
	});
});