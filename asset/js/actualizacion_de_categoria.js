$('#actualizacion').click(function () { 

	var id_categoria = $('#id_categoria').val(),
	categoria_producto = $('#categoria_producto').val();

	if(categoria_producto.length === 0){
		alert("Ingrese La Categoria");
		return false;
	}

	$.ajax({
		dataType: 'json',
		url: baseurl+'control_de_categoria/editar',
		type: 'post',
		dataType: 'json',
		data:{
			id_categoria, categoria_producto
		},
		beforesend: function(){

		},

		success: function(data){

			if(data.success === 1){

				alert("Categoria Actualizada");
				document.location.href=baseurl+'control_de_categoria/index';
			}else{
				alert('No se pudo actualizar');
			}
		},
		error: function(e){

			alert('!Algo Fallo, Intentelo Mas Tarde');
			console.log(e);
		}
	});
});

//////////////////////// PRODUCTO ///////////////////////////

$('#actualizar_producto').click(function(){ 

	var numero_barra = $('#numero_barra').val(),
	nombre_producto = $('#nombre_producto').val(),
	f_fabricacion = $('#f_fabricacion').val(),
	id_categoria = $('#id_categoria').val(),
	descripcion = $('#descripcion').val(),
	imagen = $('#imagen').val(),
	stock = $('#stock').val(),
	cantidad = $('#cantidad').val(),
	fecha_vencimiento = $('#fecha_vencimiento').val(),
	id_proveedor = $('#id_proveedor').val(),
	precio = $('#precio').val();


	$.ajax({
		dataType: 'json',
		url:baseurl+'control_de_categoria/editar_producto',
		type: 'post',
		dataType: 'json',
		data:{
			numero_barra, nombre_producto,f_fabricacion,id_categoria,descripcion, imagen, stock,
			cantidad, fecha_vencimiento, id_proveedor, precio
		},
		beforesend: function(){

		},

		success: function(data){

			if(data.success ===1){
				alert('Producto Actualizado!');
				document.location.href=baseurl+'control_de_categoria/producto';
			}else{
				alert('No Se Puede Acceder');
			}
		},
		error: function(e){
			alert('!Algo Fallo, Intentelo Mas Tarde');
			console.log(e);
		}
	});

});