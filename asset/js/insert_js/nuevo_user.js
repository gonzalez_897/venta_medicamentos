$('#insert_user').click(function(){
	var nombre = $('#nombre').val(),
		apellido = $('#apellido').val(),
		edad = $('#edad').val(),
		sexo = $('#sexo').val(),
		correo = $('#correo').val(),
		usuario = $('#usuario').val(),
		contraseña = $('#contraseña').val(),
		rol = $('#rol').val();

		 

		$.ajax({
			dataType:'json',
			url:baseurl+'control_de_usuarios/insert_usuario',
			type:'post',
			dataType:'json',
			data:{

				nombre,apellido, edad, sexo, correo, usuario,contraseña, rol

			}, 

			beforesend:function(){

			},

			success:function(data){
				if (data.success === 1) {
					alert("El usuario ha sido registrado");
					document.location.href=baseurl+'control_de_usuarios/';
				}else{
					alert("No se pudo registrar el usuario");
				}
			},

			error:function(e){
				alert("Algo fallo, Consulte con el administrador");
				console.log(e);
			}

		});

});