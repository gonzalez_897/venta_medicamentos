$('#agregar_categoria').click(function (){
    var categoria_producto = $('#categoria_producto').val();

    if(categoria_producto.length === 0){
       $('#alerta').text("este campo es obligatorio").css('color','red');
       return false;
   }
   else if(categoria_producto.length <= 5){
       $('#alerta').text("agrege mas de 5 caracteres").css('color','red');
       return false;
   }

   $.ajax({
    dataType: 'json',
    url: baseurl+'control_de_categoria/agregar',
    type: 'post',
    dataType: 'json',


    data:{
        categoria_producto
    },
    dataType: 'json',
    beforesend: function(){

    },
    success: function(data){

        if(data.success === 1){

            alert("Registros Insertados");
            document.location.href=baseurl+'control_de_categoria/index';
        }
        else{

            alert("No se pudo acceder");
        }
    },
    error: function(e){
            //alert("!!Algo Fallo, Consulte Con el Administrador");
            console.log(e);
        }
    });

});



$('#agregar_producto').click(function (){
    var numero_barra = $('#numero_barra').val(),
    nombre_producto = $('#nombre_producto').val(),
    f_fabricacion = $('#f_fabricacion').val(),
    id_categoria = $('#id_categoria').val(),
    descripcion = $('#descripcion').val(),
    imagen = $('#imagen').val(),
    stock = $('#stock').val(),
    cantidad = $('#cantidad').val(),
    fecha_vencimiento = $('#fecha_vencimiento').val(),
    id_proveedor = $('#id_proveedor').val(),
    precio = $('#precio').val();

    if(numero_barra.length === 0){
        $('#alerta').text("este campo es obligatorio").css('color','red');
        return false;
    }
    else if(numero_barra.length <= 12){
       $('#alerta').text("ingrese 13 digitos").css('color','red');

       return false;
   }

   else if(nombre_producto.length === 0){
    $('#alerta1').text('Este campo es obligatorio').css('color','red');
    return false;
}

$.ajax({
    dataType: 'json',
    url: baseurl+'control_de_categoria/agregar_producto',
    type: 'post',
    dataType: 'json',

    data:{
        numero_barra, nombre_producto, f_fabricacion, id_categoria, descripcion,
        imagen, stock, cantidad, fecha_vencimiento, id_proveedor, precio
    },

    dataType: 'json',
    beforesend: function(){

    },
    success: function(data){

        if(data.success === 1){

            alert("Registros Insertados");
            document.location.href=baseurl+'control_de_categoria/producto';
        }
        else{

            //alert("No se pudo acceder");
        }
    },
    error: function(e){
          // alert("!!Algo Fallo, Consulte Con el Administrador");
          console.log(e);
      }
  });

});

