-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-08-2019 a las 23:30:10
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_farmacia`
--
CREATE DATABASE IF NOT EXISTS `sistema_farmacia` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `sistema_farmacia`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `categoria_producto` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `categoria_producto`, `imagen`) VALUES
(1, 'Analgésicos', 'analgesicos.jpeg'),
(2, 'Antiácidos', 'antiacido.jpg'),
(3, 'antiulcerosos', 'antiulceroso.jpg'),
(4, ' Antialérgicos', 'antialergico.jpg'),
(5, 'Antidiarreicos ', 'antidiarreico.jpg'),
(7, 'Antiinfecciosos', 'antiinfeccioso.jpg'),
(8, 'Antiinflamatorios', 'antiinflamatorio.jpg'),
(9, 'Antipiréticos', 'antipiretico.jpg'),
(10, 'Antitusivos', 'antitusivo.png'),
(11, 'mucolí', 'mucolitico.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion`
--

CREATE TABLE `facturacion` (
  `numero_facturacion` int(11) NOT NULL,
  `fecha_venta` date NOT NULL,
  `hora_de_venta` time NOT NULL,
  `subtotal` float(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `facturacion`
--

INSERT INTO `facturacion` (`numero_facturacion`, `fecha_venta`, `hora_de_venta`, `subtotal`) VALUES
(5353, '2019-08-16', '08:56:34', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `numero_barra` bigint(11) NOT NULL,
  `nombre_producto` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `f_fabricacion` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `id_categoria` int(100) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `imagen` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `stock` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `precio` float(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`numero_barra`, `nombre_producto`, `f_fabricacion`, `id_categoria`, `descripcion`, `imagen`, `stock`, `cantidad`, `fecha_vencimiento`, `id_proveedor`, `precio`) VALUES
(54665, 'panadol', '2019-08-15', 1, 'alivia dolor', 'Panadol-Extra-Fuerte-408x300.jpg', '4.50', 5, '2019-08-15', 3, 5.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int(11) NOT NULL,
  `primer_nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `primer_apellido` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `telefono` int(11) NOT NULL,
  `empresa` varchar(150) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `primer_nombre`, `primer_apellido`, `correo`, `edad`, `telefono`, `empresa`) VALUES
(1, 'Josué', 'Rodriguez', 'jrodriguez@gmail.com', 32, 22876954, 'Laboratorios Cofasa'),
(2, 'Fernando', 'Avalos', 'feravalos@outlook.com', 45, 21457896, 'Laboratorios López'),
(3, 'Laura', 'Callejas', 'lcallejas@gmail.com', 25, 25784512, 'Laboratorios Suizos'),
(4, 'Robinson', 'González', 'rgonzales@gmail.com', 20, 25879541, 'Laboratorios Suizos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `rol`) VALUES
(1, 'root'),
(2, 'admin'),
(3, 'standart');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sexo`
--

CREATE TABLE `sexo` (
  `id_sexo` int(11) NOT NULL,
  `sexo` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `sexo`
--

INSERT INTO `sexo` (`id_sexo`, `sexo`) VALUES
(1, 'Masculino'),
(2, 'Femenino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `correo_electronico` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `contraseña` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre`, `apellido`, `edad`, `id_sexo`, `correo_electronico`, `usuario`, `contraseña`, `id_rol`) VALUES
(1, 'Robinson', 'Rodriguez', 19, 1, 'robinson@gmail.com', 'scrum01', '6ccf929934691710135f3f0df7cc43c5', 1),
(2, 'Yasmin', 'Espinoza', 19, 2, 'yasmin@gmail.com', 'admin01', '6ccf929934691710135f3f0df7cc43c5', 2),
(3, 'Lau', 'Callejas', 18, 2, 'lau@gmail.com', 'scrum02', '6ccf929934691710135f3f0df7cc43c5', 1),
(4, 'Diego', 'Alvarado', 19, 1, 'diego@gmail.com', 'cajero', '6ccf929934691710135f3f0df7cc43c5', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id_venta` int(11) NOT NULL,
  `numero_barra` bigint(11) NOT NULL,
  `producto` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `precio` float(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` float(10,2) NOT NULL,
  `f_venta` date NOT NULL,
  `numero_facturacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id_venta`, `numero_barra`, `producto`, `precio`, `cantidad`, `total`, `f_venta`, `numero_facturacion`) VALUES
(16, 54665, 'panadol', 5.00, 11, 55.00, '2019-08-16', 7674),
(17, 54665, 'panadol', 5.00, 1222, 6110.00, '2019-08-16', 7674),
(18, 54665, 'panadol', 5.00, 11, 55.00, '2019-08-16', 4192),
(19, 54665, 'panadol', 5.00, 111, 555.00, '2019-08-16', 4192);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `facturacion`
--
ALTER TABLE `facturacion`
  ADD PRIMARY KEY (`numero_facturacion`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`numero_barra`),
  ADD KEY `categoria` (`id_categoria`),
  ADD KEY `proveedores` (`id_proveedor`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `sexo`
--
ALTER TABLE `sexo`
  ADD PRIMARY KEY (`id_sexo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `rol` (`id_rol`),
  ADD KEY `sexo` (`id_sexo`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`,`numero_facturacion`),
  ADD KEY `venta` (`numero_barra`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `facturacion`
--
ALTER TABLE `facturacion`
  MODIFY `numero_facturacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5354;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sexo`
--
ALTER TABLE `sexo`
  MODIFY `id_sexo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `proveedores` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id_proveedor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sexo` FOREIGN KEY (`id_sexo`) REFERENCES `sexo` (`id_sexo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`numero_barra`) REFERENCES `producto` (`numero_barra`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
