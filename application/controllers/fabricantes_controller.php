<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');




class fabricantes_controller extends CI_controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('fabricantes_model');
	}

	public function index()
	{
		$data = array(
			'page_title' => 'Laboratorios',
			'view' => 'fabricantes_view',
			'data_view' => array()
		);
		$this->load->view('template/main',$data);
	}
}


 ?>