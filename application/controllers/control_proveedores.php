<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class control_proveedores extends CI_Controller {

	public function __construct(){

		Parent::__construct();
		$this->load->model('modelo_proveedor');
	}


	public function proveedor()
	{

		if ($this->session->userdata('is_logued_in') === TRUE) {
			$data= array(
				'page_title'=>'proveedor',
				'view'=>'proveedor_view',
				'data_view'=>array()

			);

			$proveedor=$this->modelo_proveedor->obtener_proveedor();
			$data['proveedor']= $proveedor;
			$this->load->view('template/main_view',$data);
		}else{
			$this->load->view('login/login_view');
		}

		
	} 


	public function agregar_proveedor()
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {
			$data= array(
				'page_title'=>'proveedor',
				'view'=>'form/ingreso_proveedor',
				'data_view'=>array()

			);

			$proveedor=$this->modelo_proveedor->obtener_proveedor();
			$data['proveedor']= $proveedor;
			$this->load->view('template/main_view',$data);
		}else{
			$this->load->view('login/login_view');
		}

		
	}
	public function agregar_proveedores(){
		if ($this->session->userdata('is_logued_in') === TRUE) {
			if ($this->input->is_ajax_request()) {
				$data = array(

					'primer_nombre' => $this->input->post('primer_nombre'),
					'primer_apellido' => $this->input->post('primer_apellido'),
					'correo'=> $this->input->post('correo'),
					'edad'=> $this->input->post('edad'),
					'telefono'=> $this->input->post('telefono'),
					'empresa'=> $this->input->post('empresa')



				);
				if($this->modelo_proveedor->insert($data)) {

					echo json_encode(array('success'=> 1));			
				}else{
					echo json_encode(array('success'=> 0));
				}
			}else{
				echo "No se puede acceder";
			}
		}else{
			$this->load->view('login/login_view');
		}


	}
	public function actualizar_proveedores(){
		if ($this->session->userdata('is_logued_in') === TRUE) {
			if ($this->input->is_ajax_request()) {

				$data = array(
					'id_proveedor' => $this->input->post('id_proveedor'),
					'primer_nombre' => $this->input->post('primer_nombre'),
					'primer_apellido' => $this->input->post('primer_apellido'),
					'correo'=> $this->input->post('correo'),
					'edad'=> $this->input->post('edad'),
					'telefono'=> $this->input->post('telefono'),
					'empresa'=> $this->input->post('empresa')



				);

				if($this->modelo_proveedor->update($data)) {

					echo json_encode(array('success'=> 1));			
				}else{
					echo json_encode(array('success'=> 0));
				}
			}else{

				echo "no se puede acceder";
			}
		}else{
			$this->load->view('login/login_view');
		}
		


	}
	
	


	
	public function actualizar_proveedor($id)
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {
			$data= array(
				'page_title'=>'proveedor',
				'view'=>'form/actualizar_proveedor',
				'data_view'=>array()

			);

			$proveedor=$this->modelo_proveedor->act_proveedor($id);
			$data['proveedor']= $proveedor;

			$this->load->view('template/main_view',$data);
		}else{
			$this->load->view('login/login_view');
		}
		
	}

	public function delete_pro($id)
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {
			$this->modelo_proveedor->delete_prove($id);
			$this->proveedor();



		}else{
			$this->load->view('login/login_view');
		}
	}




}


?>

