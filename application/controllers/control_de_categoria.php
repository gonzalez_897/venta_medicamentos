<?php 
defined('BASEPATH')OR exit('No direct script access allowed');

class control_de_categoria extends CI_controller{
	
	public function __construct(){
		parent:: __construct();
		$this->load->model('categoria_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('pdf');
		
		$this->_mcrypt_exists = ( ! function_exists('mcrypt_encrypt')) ? FALSE : TRUE;
	}

	public function index(){

		if ($this->session->userdata('is_logued_in') === TRUE) {

			$data = array(
				'page_title' => 'Registro de categoria',
				'view' => 'form/administracion_categoria',
				'data_view' => array()

			);
			$categoria =$this->categoria_model->categoria();
			$data['categoria'] = $categoria;
			$this->load->view('template/main_view',$data);

		}else{
			$this->load->view('login/login_view');
		}

	}


	public function eliminar($id)
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {

			$this->categoria_model->eliminar($id);
			redirect('control_de_categoria');

		}else{
			$this->load->view('login/login_view');
		}
	}

	public function agregar_dato(){

		if ($this->session->userdata('is_logued_in') === TRUE) {

			$data = array(
				'page_title' => 'Ingreso de categorias',
				'view' => 'form/ingreso_categoria',
				'data_view' => array()
			);
			$this->load->view('template/main_view',$data);

		}else{
			$this->load->view('login/login_view');
		}
	}

	public function agregar(){

		if ($this->session->userdata('is_logued_in') === TRUE) {

			$this->load->helper(array('form','url'));

			$this->load->library('form_validation');

			if ($this->input->is_ajax_request()) {

				$this->form_validation->set_rules('categoria_producto', 'Categoria','required');
				$data = array(
					array(
						'field'=> 'categoria_producto',
						'label' => 'Categoria',
						'rules' => 'required'
					)
				);

				$this->form_validation->set_rules($data);

				if($this->form_validation->run() == FALSE){
					$this->load->view('form/ingreso_categoria');
				}else{

					$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

					$this->load->model('categoria_model');
				}

				$data = array(
					'categoria_producto' => $this->input->post('categoria_producto')
				);
				if($this->categoria_model->insert($data)) {

					echo json_encode(array('success'=> 1));			
				}else{
					echo json_encode(array('success'=> 0));
				}
			}else{ 

				echo "no se puede acceder";
			}

		}else{
			$this->load->view('login/login_view');
		}

	}

	public function accion($id_categoria){

		if ($this->session->userdata('is_logued_in') === TRUE) {

			$data = array(
				'page_title' => 'Actualizacion de categorias',
				'view' => 'update/actualizacion_categoria',
				'data_view' =>array()
			);

			$categoria = $this->categoria_model->obtener($id_categoria);
			$data['categoria'] = $categoria;
			$this->load->view('template/main_view',$data);

		}else{
			$this->load->view('login/login_view');
		}

	}


	public function editar(){

		if ($this->session->userdata('is_logued_in') === TRUE) {

			if($this->input->is_ajax_request())
			{

				$data = array(
					'id_categoria' => $this->input->post('id_categoria'),
					'categoria_producto' => $this->input->post('categoria_producto')
				);
				if ($this->categoria_model->actualizar($data))
				{
					echo json_encode(array('success' => 1));
				}
				else
				{
					echo json_encode(array('success' => 0));
				}	
			}
			else
			{
				echo 'no se puede acceder';
			}

		}else{
			$this->load->view('login/login_view');
		}
	}

	///////////////////////////////// PRODUCTO /////////////////////////////////

	public function producto(){

		if ($this->session->userdata('is_logued_in') === TRUE) {

			$data = array(

				'page_title' => 'Registro de productos',
				'view' => 'form/administracion_productos',
				'data_view' => array()

			);
			$producto =$this->categoria_model->producto();
			$data['producto'] = $producto;
			$this->load->view('template/main_view',$data);
		}else{
			$this->load->view('login/login_view');
		}
	}

	public function eliminar_producto($id){
		$this->categoria_model->eliminar_producto($id);
		redirect('control_de_categoria/producto');
	}

	public function agregar_dato_producto(){
		if ($this->session->userdata('is_logued_in') === TRUE) {

			$data = array(
				'page_title' => 'Ingreso de Productos',
				'view' => 'form/ingreso_producto',
				'data_view' => array()
			);
			$producto =$this->categoria_model->producto();
			$data['producto'] = $producto;
			$categoria = $this->categoria_model->categoria();
			$data['categoria'] =$categoria;
			$proveedor = $this->categoria_model->proveedor();
			$data['proveedor'] =$proveedor;
			if($this->load->view('template/main_view.php',$data));

		}else{
			$this->load->view('login/login_view');
		}
	}

	public function agregar_producto(){
		if ($this->session->userdata('is_logued_in') === TRUE) {

			if($this->input->is_ajax_request()){
				$data = array(

					'numero_barra' => $this->input->post('numero_barra'),
					'nombre_producto' => $this->input->post('nombre_producto'),
					'f_fabricacion' => $this->input->post('f_fabricacion'),
					'id_categoria' => $this->input->post('id_categoria'),
					'descripcion' => $this->input->post('descripcion'),
					'imagen' => $this->input->post('imagen'),
					'stock' => $this->input->post('stock'),
					'cantidad' => $this->input->post('cantidad'),
					'fecha_vencimiento' => $this->input->post('fecha_vencimiento'),
					'id_proveedor' => $this->input->post('id_proveedor'),
					'precio' => $this->input->post('precio')
				);

				if($this->categoria_model->insertar_producto($data)){
					echo json_encode(array('success' => 1));
				}else{
					echo json_encode(array('success' => 0));
				}	
			}else{
				echo 'No se puede acceder';
			}
		}else{
			$this->load->view('login/login_view');
		}

	}

	public function accion_producto($numero_barra){
		if ($this->session->userdata('is_logued_in') === TRUE) {


			$data = array(
				'page_title' => 'actualizar producto',
				'view' => 'update/actualizacion_de_producto_view',
				'data_view' => array()
			);

			$categoria = $this->categoria_model->categoria();
			$data['categoria'] =$categoria;
			$proveedor = $this->categoria_model->proveedor();
			$data['proveedor'] =$proveedor;

			$producto = $this->categoria_model->obtener_producto($numero_barra);
			$data['producto'] = $producto;
			$this->load->view('template/main_view',$data);

		}else{
			$this->load->view('login/login_view');
		}
	}

	public function editar_producto(){

		if ($this->session->userdata('is_logued_in') === TRUE) {


			if($this->input->is_ajax_request()){

				$data = array(
					'numero_barra' => $this->input->post('numero_barra'),
					'nombre_producto' => $this->input->post('nombre_producto'),
					'f_fabricacion' => $this->input->post('f_fabricacion'),
					'id_categoria' => $this->input->post('id_categoria'),
					'descripcion' => $this->input->post('descripcion'),
					'imagen' => $this->input->post('imagen'),
					'stock' => $this->input->post('stock'),
					'cantidad' => $this->input->post('cantidad'),
					'fecha_vencimiento' => $this->input->post('fecha_vencimiento'),
					'id_proveedor' => $this->input->post('id_proveedor'),
					'precio' => $this->input->post('precio')
				);
				if($this->categoria_model->actualizar_producto($data)){

					echo json_encode(array('success' => 1));
				}
				else
				{
					echo json_encode(array('success' => 0));
				}	
			}
			else
			{
				echo 'no se puede acceder';
			}
		}else{
			$this->load->view('login/login_view');
		}

	}

	public function detalle_pdf()

	{

		ob_start();
		$producto = $this->categoria_model->producto();
		$data['producto'] = $producto;
		$this->load->view('datos_producto_pdf',$data);

		$paper_size = array(0,0,360,756.00);
		$this->pdf->setPaper ($paper_size); 
		$this->pdf->loadhtml(ob_get_clean());
		$this->pdf->render();
		$this->pdf->stream("pdf", array("Attachment"=>0));



	}

}
?>