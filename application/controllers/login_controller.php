<?php 
defined('BASEPATH') OR exit('No direc script access allowed');
class login_controller extends CI_Controller{ 




	public function __construct(){

		parent::__construct();
		$this->load->model('login_model');
		$this->load->library('form_validation');
		$this->load->helper('form');
		
	}
	public function inicio(){
		if ($this->session->userdata('is_logued_in') === TRUE) {
			$data = array(
				'page_title'=>'Pharmacy Store',
				'view'=>'index_view',
				'data_view'=>array()
			);
			$categoria =$this->login_model->categoria();
			$data['categoria'] = $categoria;
			$this->load->view('template/main_view',$data);
		}else{
			$this->load->view('login/login_view');
		}
		
	}

	public function index(){
		
		$this->load->view('login/login_view');
	}

	

	public function categoria(){

		$data = array(
			'page_title' => 'categorias',
			'view' => 'index_view',
			'data_view' => array()

		);
		$categoria =$this->login_model->categoria();
		$data['categoria'] = $categoria;
		$this->load->view('template/main_view',$data);
	}


	 

	public function auth()
	{
		$usuario = $this->input->post('usuario',TRUE);
		$contraseña = md5($this->input->post('contraseña',TRUE));
		$validacion = $this->login_model->get_user($usuario,$contraseña);

		if ($validacion->num_rows() > 0) {
			$data = $validacion->row_array();
			$id_usuario = $data['id_usuario'];
			$usuario = $data['usuario'];
			$contraseña = $data['contraseña'];
			$id_rol = $data['id_rol'];

			$session_data = array(
				'id_usuario' => $id_usuario,
				'usuario' => $usuario,
				'contraseña' => $contraseña,
				'id_rol' => $id_rol,
				'is_logued_in' => TRUE 

			);

			$this->session->set_userdata($session_data);
			if ($id_rol === '1' ) {
				redirect('login_controller/inicio');
			}elseif($id_rol === '2'){
				redirect('login_controller/inicio');
			}elseif ($id_rol === '3') {
				redirect('login_controller/inicio');
			}

		}else{
			echo $this->session->set_flashdata('msg','Usuario o contraseña Incorrectos');
			redirect('login_controller');
		}

	}


	public function logout(){

		if ($this->session->userdata('is_logued_in') == TRUE ) {

			$this->session->sess_destroy();
			redirect(base_url().'login_controller/index');
		}


	} 

}


?>