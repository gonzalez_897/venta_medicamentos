<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Control_de_usuarios extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('usuarios');
		$this->load->library('form_validation');
		$this->load->library('pdf');
	}

	public function index()
	{ 
		if ($this->session->userdata('is_logued_in') === TRUE) {
			$data = array(
				'page_title' => 'Registro de usuarios',
				'view' => 'form/administracion_usuarios',
				'data_view' => array()
			);

			$usuario = $this->usuarios->get_usuarios();
			$data['usuario'] = $usuario;

			$this->load->view('template/main_view',$data);
		}else{
			$this->load->view('login/login_view');
		}


		
	} 


	public function usuarios()
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {
			$data = array(
				'page_title' => 'Ingreso de usuarios',
				'view' => 'form/ingreso_usuarios',
				'data_view' => array()
			);

			$sexo = $this->usuarios->sexo();
			$data['sexo'] = $sexo;

			$rol = $this->usuarios->rol();
			$data['rol'] = $rol;

			$this->load->view('template/main_view',$data);
		}else{
			
			
			$this->load->view('login/login_view');

			
		}


	} 




	public function insert_usuario()
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {
			if ($this->input->is_ajax_request()) {
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'edad' => $this->input->post('edad'),
					'id_sexo' => $this->input->post('sexo'),
					'correo_electronico' => $this->input->post('correo'),
					'usuario' => $this->input->post('usuario'),
					'contraseña' => md5($this->input->post('contraseña')),
					'id_rol' => $this->input->post('rol')

				);

				if ($this->usuarios->insert_user($data)) {
					echo json_encode(array('success' => 1));
				}else{
					echo json_encode(array('success' => 0));
				}
			}else{
				echo "No se puede acceder";
			}
		}else{
			$this->load->view('login/login_view');
		}
		
	}



	public function insert($data)
	{
		$this->usuarios->insert_user($data);
	}


	public function update_user($id)
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {
			$data = array(
				'page_title' => 'Actualizacion de usuarios',
				'view' => 'update/actualizacion_usuarios',
				'data_view' => array()
			);

			$sexo = $this->usuarios->sexo();
			$data['sexo'] = $sexo;

			$rol = $this->usuarios->rol();
			$data['rol'] = $rol;

			$data['user'] = $this->usuarios->all_user($id);

			$this->load->view('template/main_view',$data);
		}else{
			$this->load->view('login/login_view');
		}

		
	} 

	public function actualizar_usuarios()
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {
			if ($this->input->is_ajax_request()) {
				$data = array(
					'id_usuario' => $this->input->post('id_usuario'),
					'nombre' => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'edad' => $this->input->post('edad'),
					'id_sexo' => $this->input->post('sexo'),
					'correo_electronico' => $this->input->post('correo'),
					'usuario' => $this->input->post('usuario'),
					'contraseña' => $this->input->post('contraseña')
				);
				if ($this->usuarios->update_user($data)) {
					
					echo json_encode(array('success' => 1));
				}else{
					echo json_encode(array('success' => 0));
				}
			}else{
				echo "No se puede acceder";
			}
		}else{
			$this->load->view('login/login_view');
		}
	}







	public function delete_user($id)
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {
			
			if ($this->usuarios->del_user($id)) {
				header('refresh:0,../');
			}elseif ($this->session->userdata('id_usuario') === $id ) {

				echo "<script>
				alert('No puede eliminar su usuario');
				</script>";


				header('refresh:0,../');


			}else{
				echo 
				"<script>
				alert('No puedes eliminar todos los usuarios');
				</script>";

				header('refresh:0,../');

			}
		}else{
			$this->load->view('login/login_view');
		}
		
	}

	
	public function detalle_pdf()

	{

		ob_start();
		$usu = $this->usuarios->get_usu();
		$data['usu'] = $usu;
		$this->load->view('datos_pdf',$data);
		
		$paper_size = array(0,0,360,756.00);
		$this->pdf->setPaper ($paper_size); 
		$this->pdf->loadhtml(ob_get_clean());
		$this->pdf->render();
		$this->pdf->stream("pdf", array("Attachment"=>0));
		
		
		
	} 



}
