<?php
defined('BASEPATH') OR exit ('No direct script access allowed');




class control_de_ventas extends CI_controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('ventas');
	}

	public function index()
	{

		if ($this->session->userdata('is_logued_in') === TRUE) {


			$data = array(
				'page_title' => 'Ventas',
				'view' => 'form/lista_venta',
				'data_view' => array()
			);
			$ventas = $this->ventas->get_ventas();
			$data['ventas'] = $ventas;
			$this->load->view('template/main_view',$data);

		}else{
			$this->load->view('login/login_view');
		}

	}

	public function agregar_ventas()
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {

			$data = array(
				'page_title' => 'Ventas',
				'view' => 'form/ingreso_venta',
				'data_view' => array()
			);
			$productos = $this->ventas->productos();
			$data['productos'] = $productos;
			$this->load->view('template/main_view',$data);


		}else{
			$this->load->view('login/login_view');
		}
	}

	public function get_ventas($factura)
	{
		if ($this->session->userdata('is_logued_in') === TRUE) {

			$data = array(
				'page_title' => 'Muestra',
				'view' => 'form/lista_venta2',
				'data_view' => array()
			);
			$dato['factura'] = $factura;
			$data['ventas'] = $this->ventas->mostrar_producto($dato);
			$this->load->view('template/main_view',$data);

		}else{
			$this->load->view('login/login_view');
		}
	}


	public function set_ventas()


	{
		if ($this->session->userdata('is_logued_in') === TRUE) {
			

			$codigo = $this->input->post('codigo');
			$cantidad = $this->input->post('cantidad');
			$total = $this->input->post('total');
			$fecha = $this->input->post('fecha');
			$nombre = $this->input->post('nombre');
			$precio = $this->input->post('precio');
			$factura = $this->input->post('factura');


			for ($i=0; $i < count($codigo); $i++) { 

				$data = array(
					'numero_barra' => $codigo[$i],
					'producto' => $nombre[$i],
					'precio' => $precio[$i],
					'cantidad' => $cantidad[$i],
					'total' => $total[$i],
					'f_venta' => $fecha,
					'numero_facturacion' => $factura
				);
				$this->ventas->set_ventas($data);
			}
			redirect('control_de_ventas/');

		}else{
			$this->load->view('login/login_view');
		}
		
	}

}

?>