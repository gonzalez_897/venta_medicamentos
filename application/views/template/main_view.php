<!DOCTYPE html>
<html>
<head>
	<title><?=$page_title ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/datatables.min.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/datatables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/style.css">

</head>
<body>
 
	<?php $this->load->view('template/header_view'); ?>
	<?php $this->load->view($view,$data_view); ?>
	<?php $this->load->view('template/footer_view'); ?>
 
</body>
</html>
<link rel="stylesheet" href="<?php echo base_url().'asset/icon/css/all.css';?>">
<!-- Load font awesome icons -->

<script src="<?php echo base_url();?>asset/mdb/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/mdb/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>asset/mdb/js/mdb.min.js"></script>

<script src="<?php echo base_url();?>asset/js/main.js"></script>


<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Gudea&display=swap" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url('asset/css/main.css') ?>">
<script type="text/javascript">
	$(document).ready(function () {
		$('#ingreso').click(function () {
			if ($('#producto').val().length === 0) {
				$('#alerta').text("No Ha seleccionado un producto");
				$('#alerta2').text("");
				return false;
			}else{
				$('#alerta').text("");
			}
			if ($('#cantidad').val().length === 0) {
				$('#alerta2').text("Porfavor ingresar cantidad");
				return false;
			}else{
				$('#alerta2').text("");
			}
			var productos = $('#nombre').val(),
			codigo = $("#codigo").val(),
			cantidad = $('#cantidad').val(),
			precio = $('#precio').val(),
			total = Number(cantidad * precio).toFixed(2);
			lista = '<tr align="center">'
			+'<td><input type="hidden" name="codigo[]" value='+codigo+'>'+codigo+'</td>'
			+'<td><input type="hidden" name="nombre[]" value='+productos+'>'+productos+'</td>'
			+'<td>$<input type="hidden" name="precio[]" value='+precio+'>'+precio+'</td>'
			+'<td><input type="hidden" name="cantidad[]" value='+cantidad+'>'+cantidad+'</td>'
			+'<td><input type="hidden" name="total[]" value='+total+'>'+total+'</td>'
			+'<td>'+'<button type="button" class="btn btn-danger eliminar" id='+productos+'>Eliminar</button>'+'</td>'
			+'</tr>';
			$('#lista').append(lista);
			resultado();
			$('#ingreso').val(null);
			$('#producto').val(null);
			$('#imagen').remove();
			$('#cantidad').val(null);
			$('#tabla').on("click",".eliminar",function () {
				$(this).parents("tr").remove();
				$('#ingreso').val(null);
				resultado();
			});
		});
		function resultado() {
			consulta = 0;
			$('#tabla tbody tr').each(function () {
				consulta = consulta + Number($(this).find("td:eq(4)").text());
			});
			$('#resultado').text(consulta.toFixed(2));

		}
		$("#producto").on('change',function () {
			var valores = $(this).val();

			if (valores != "") {
				espacio = valores.split(",");

				$("#precio").val(espacio[1]);
				$("#nombre").val(espacio[2]);
				$("#codigo").val(espacio[0]);
				$(".imagen").html('<img width="290" id="imagen" height="190" src=<?php echo base_url(); ?>asset/imagen/'+espacio[3]+'></img>');
			}else{
				$(".imagen").text("");
				$("#precio").val(null);
				$("#nombre").val(null);
				$("#codigo").val(null);
			}
		});

	});
</script>

