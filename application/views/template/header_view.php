<!DOCTYPE html>
<html>
<head>
  <link href="https://fonts.googleapis.com/css?family=Didact+Gothic&display=swap" rel="stylesheet">
  <title></title>
  <script type="text/javascript">

 /*  if(window.history.forward(1) != null) window.history.forward(1); */
       if (history.forward(1)) {
      location.replace(history.forward(1));
        
    }

  </script>

</head>

<body>
    <div style="margin-left: 250px;margin-bottom: -60px; margin-top: 40px;color: #687E8C;font-family: 'Didact Gothic', sans-serif;"><h2>Al Servicio de su Salud</h2></div>
  <div align="left">
<img style="height: 100px; width: 200px; margin-top: -25px" src="<?php echo base_url('asset/imagen/logo.png') ?>">
<img style="height: 50px; width: 200px; margin-top: -25px;margin-left:335px" src="<?php echo base_url('asset/imagen/heart.png') ?>">
</div>
<div style="background-color: white;height: 100px;margin-top: -100px"></div>
   <nav style="background-image: linear-gradient(#27697F, #26BFBF); width: 100%;" class="navbar navbar-expand-lg">
   
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <div class="collapse navbar-collapse" id="navbarNav">
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>login_controller/inicio"><i class="fas fa-home"></i> Inicio</a>
                </li>

                <?php if ($this->session->userdata('id_rol') === '1') { ?>

                  <li class="nav-item active">
                    <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_de_usuarios/index"><i class="fas fa-user-friends"></i> Usuarios</a>
                  </li>
                  <li class="nav-item active">
                    <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_de_categoria/producto"><i class="fas fa-list"></i> Productos</a>
                  </li>
                  <li class="nav-item active">

                    <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_proveedores/proveedor"><i class="fas fa-truck"></i> Proveedores</a>
                    

                  </li>
                  <li class="nav-item active">
                    <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_de_ventas/index"><i class="fas fa-dollar-sign"></i> Ventas</a>
                  </li>
                  <li class="nav-item active">
                    <a style="font-size:16px;color:white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_de_categoria/index"><i class="fas fa-folder"></i> Categorias</a>
                  </li>

                    </div>
                  <div align="right" style=" margin-top: 8px;margin-bottom:8px; margin-left: 400px;">
                      <button style="color: white;font-size: 16px; font-family: arial" class="btn"><a style="color:white; text-decoration: none;" href="<?php echo base_url(); ?>login_controller/logout"><i class="fas fa-user"></i> Cerrar Sesion <?=$this->session->userdata('usuario'); ?></a></button>

                    </div>

                  <?php }elseif ($this->session->userdata('id_rol') === '2') { ?>

                    <li class="nav-item active">
                      <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_de_usuarios/index"><span class=" icon-users"> Usuarios</span></a>
                    </li>
                    <li class="nav-item active">
                      <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_de_categoria/producto"><span class="icon-file-text2"> Productos</a>
                      </li>

                      <li class="nav-item active">
                        <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_de_ventas/index"><span class="icon-files-empty"> Ventas</a>
                        </li>
                              </ul>
                            </div>
                            <div align="right" style=" margin-top: 8px;margin-bottom:8px; margin-left: 690px;">
                              <button style="color: white;font-size: 16px; font-family: arial" class="btn"><a style="color:white; text-decoration: none;" href="<?php echo base_url(); ?>login_controller/logout"><i class="fas fa-user"></i> Cerrar Sesion <?=$this->session->userdata('usuario'); ?></a></button>

                            </div>
                          <?php }elseif ($this->session->userdata('id_rol') === '3') { ?>

                            <li class="nav-item active">
                              <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_de_categoria/producto"><span class="icon-file-text2"> Productos</a>
                              </li>
                              <li class="nav-item active">
                                <a style="font-size: 16px;color: white;font-family:arial" class="nav-link" href="<?php echo base_url(); ?>control_de_ventas/index"><span class="icon-files-empty"> Ventas</a>
                                </li>
                                    </ul>
                                  </div>
                                  <div align="right" style=" margin-top: 8px;margin-bottom:8px; margin-left: 775px;">
                                    <button style="color: white;font-size: 16px; font-family: arial" class="btn"><a style="color:white; text-decoration: none;" href="<?php echo base_url(); ?>login_controller/logout"><i class="fas fa-user"></i> Cerrar Sesion <?=$this->session->userdata('usuario'); ?> </a></button>

                                  </div>
                                <?php } ?>
                              </div>
                            </div>
                          </div>
                        </nav>
 
<div style="height: 40px"></div>
                      </body>
                      </html>

