<!DOCTYPE html>
<html>
<head>
  <title></title>
  <style type="text/css">
    html,body {
  margin: 0;
  padding: 0;
  font-family: Arial, Helvetica, Sans-serif;
  
}
.dummy_page {
  height: 600px;
  width: 100%;
  background-color: ;
  text-align: center;
  box-sizing: border-box;
  padding: 10px 0px;
}
/* STYLES SPECIFIC TO FOOTER  */

.footer {
  width: 100%;
  position:absolute ;
  height: auto;
  background-image: linear-gradient(#27697F, black);
}
.footer .col {
  width: 190px;
  height: auto;
  float: left;
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  padding: 0px 20px 20px 20px;
}
.footer .col h1 {
  margin: 0;
  padding: 0;
  font-family: inherit;
  font-size: 12px;
  line-height: 17px;
  padding: 20px 0px 5px 0px;
  color: rgba(255,255,255,0.2);
  font-weight: normal;
  text-transform: uppercase;
  letter-spacing: 0.250em;
}
.footer .col ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
.footer .col ul li {
  color: #999999;
  font-size: 14px;
  font-family: inherit;
  font-weight: bold;
  padding: 5px 0px 5px 0px;
  cursor: pointer;
  transition: .2s;
  -webkit-transition: .2s;
  -moz-transition: .2s;
}
.social ul li {
  display: inline-block;
  padding-right: 5px !important;
}

.footer .col ul li:hover {
  color: #ffffff;
  transition: .1s;
  -webkit-transition: .1s;
  -moz-transition: .1s;
}
.clearfix {
  clear: both;
}
@media only screen and (min-width: 1280px) {
  .contain {
    width: 1200px;
    margin: 0 auto;
  }
}
@media only screen and (max-width: 1139px) {
  .contain .social {
    width: 1000px;
    display: block;
  }
  .social h1 {
    margin: 0px;
  }
}
@media only screen and (max-width: 950px) {
  .footer .col {
    width: 33%;
  }
  .footer .col h1 {
    font-size: 14px;
  }
  .footer .col ul li {
    font-size: 13px;
  }
}
@media only screen and (max-width: 500px) {
    .footer .col {
      width: 50%;
    }
    .footer .col h1 {
      font-size: 14px;
    }
    .footer .col ul li {
      font-size: 13px;
    }
}
@media only screen and (max-width: 340px) {
  .footer .col {
    width: 100%;
  }
}
  </style>
</head>
<body>
  <div class="dummy_page">
 
</div>
<!-- FOOTER START -->
<div class="footer">
  <div class="contain">
  <div class="col">
    <h1>Quiénes somos</h1>
    <ul>
      <li>Somos una empresa visionaria 
        que busca innovar constantemente para la satisfacción de sus clientes</li>
    
  </div>
  <div class="col">
    <h1>Teléfonos</h1>
    <ul>
      <li>Call center: 2267-6767</li>
      <li>2267-6768</li>
    </ul>
  </div>
  <div class="col">
    </ul>
    <h1>Dirección</h1>
    <ul>
      <li>19 Avenida norte entre 3a calle poniente y Alameda Juan Pablo II, San Salvador CP</li>
      </ul>
    
  </div>
  <div class="col">
    <h1>Copyright</h1>
    <ul>
      <li>Derechos Reservados 2019 pharmacystore, S.A de C.V | Uso y condiciones | Política de Privacidad</li>
      <li>© 2019 Copyright:pharmacystore.com</li>
    </ul>
  </div>
  <div class="col">
    <h1></h1>
    <ul>
      <li>Contactanos</li>
      <li>Web chat</li>
      
    </ul>
  </div>
  <div class="col social">
    <h1>Redes Sociales</h1>
    <ul>
      <li><img src="<?php echo base_url('asset/redes/face.png') ?>" width="32" style="width: 32px;"></li>
      <li><img src="<?php echo base_url('asset/redes/whats.png') ?>" width="32" style="width: 32px;"></li>
      <li><img src="<?php echo base_url('asset/redes/instagram.png') ?>" style="width: 32px;"></li>
    </ul>
  </div>
<div class="clearfix"></div>
</div>
</div>
<!-- END OF FOOTER -->

</body>
</html>