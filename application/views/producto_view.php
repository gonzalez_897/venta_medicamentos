<!DOCTYPE html>
<html>
<head>
	<title>producto</title>
	<script type="text/javascript" src="asset/js/jquery-3.4.1.min.js"></script>
	<style type="text/css">
		table{
			position: relative;
			top:50px;
			left:0px;
			border-radius: ;
			font-weight: normal;
			padding: 3px 5px;
			padding-top: 20px;
			padding-right: 10px;
			padding-bottom: 50px;
			padding-left: 80px;
			font-size: 13pt;
			text-align: center;
			padding: 20px 0 0;
			margin: -1em 0 0;
			height: 90px;
		}
	</style>
</head>
<body style="background-color: #F2F2F2">
	<div align="center">
		<div class="container">
			<h3>Tipos de Productos</h3>
			<a class="btn btn-success" href="<?php echo base_url();?>control_de_categoria/agregar_dato_producto">Agregar Datos</a>
			<table align="center" class="table table-bordered table-hover" style="width: 1200px">
				<thead style="background-color: #26BFBF" >
			 <h3 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Registro de Productos</h3>
				<tr>
					<thead>
						<td>Numero De Barra</td>
						<td>Producto</td>
						<td>Fecha De Fabricacion</td>
						<td>Categoria</td>
						<td>Descripcion</td>
						<td>Imagen</td>
						<td>Stock</td>
						<td>Cantidad</td>
						<td>Fecha De Vencimiento</td>
						<td>Proveedor</td>
						<td>Precio</td>
						<td>Opcion</td>
						<td>Opcion</td>
					</thead>
				</tr>

				<tbody>
					<?php foreach ($producto as $P): ?>
						<tr>
							<td><?=$P->numero_barra?></td>
							<td><?=$P->nombre_producto?></td>
							<td><?=$P->f_fabricacion?></td>
							<td><?=$P->categoria_producto?></td>
							<td><?=$P->descripcion?></td>
							<td><?=$P->imagen?></td>
							<td><?=$P->stock?></td>
							<td><?=$P->cantidad?></td>
							<td><?=$P->fecha_vencimiento?></td>
							<td><?=$P->proveedor?></td>
							<td><?=$P->precio?></td>
							<td><a href="<?php echo base_url().'control_de_categoria/eliminar_producto/'.$P->numero_barra?>"><img src="<?php echo base_url();?>asset/imagen/trash.png" 
								style="height: 45px; width: 45px"></a></td>
								<td><a href="<?php echo base_url().'control_de_categoria/accion_producto/'.$P->numero_barra ?>"><img src="<?php echo base_url();?>asset/imagen/refresh.png" style="height: 45px; width: 45px"></a></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				
			</div>
		</div>
	</body>
	</html>