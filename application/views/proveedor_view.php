<!DOCTYPE html>
<html>
<head>
	<title>Proveedores</title>
	<script type="text/javascript">

		history.pushState(null, "", "http://localhost/venta_medicamentos/control_proveedores/proveedor");

		$('#box').click(function(){
			$(this).parent().slideup();
		});


	</script>
	<style type="text/css">
		.sombra{
			width:-12px;
			opacity: 0.50px; 
			box-shadow: 8px 15px 8px 10px #a1a1a1;
		}
	</style>
</head>
<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
<div align="left">
	<body style="background-color: #F2F2F2" >
				<h1 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Registro de Proveedores</h1>
				<br>
			</div>
			<div >
			<div style="background-color: white; height: 350px;width: 1250px;margin-left:80px;border-radius: 12px;border-color:black;border: 1px ">
			<table align="center" class="table table-bordered table-hover " style="width: 1200px;">
			<thead style="background-color: #26BFBF">
			<tr class="text-center" style="color: white">
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Correo</th>
			<th>Edad</th>
			<th>Teléfono</th>
			<th>Empresa</th>
			<th>Eliminar</th>
			<th>Actualizar</th>
			</tr>

		</thead>
		<?php 	foreach ($proveedor as $p){?>
			<tr class="text-center" style="color:#687E8C">
				<td><?=$p->primer_nombre?></td>
				<td><?=$p->primer_apellido?></td>
				<td><?=$p->correo?></td>
				<td><?=$p->edad?></td>
				<td><?=$p->telefono?></td>
				<td><?=$p->empresa?></td>
				<td id="box" align="center">
				<a onclick="return confirm('¿Estas seguro de que quieres eliminar el proveedor?')? slideup():false; " href="<?php echo base_url().'control_proveedores/delete_pro/'.$p->id_proveedor ?>"><i class="fas fa-trash fa-lg" style="color:#26BFBF"></i></a>
				</td>

				<td align="center">
					<a href="<?php echo base_url('control_proveedores/actualizar_proveedor/').$p->id_proveedor ?>"><i class="fas fa-edit fa-lg" style="color:#26BFBF"></i></a>
				</td>
			</tr>
		<?php } ?>
		<br><br>
		</div>
		<div style="margin-left: 30px">
			<button style="background-color:#F2A444" class="btn"><a style="text-decoration: none; color:white" href="<?php echo base_url(); ?>control_proveedores/agregar_proveedor">Nuevo Proveedor<i class="fas fa-user-plus"></i> </a></button>
		</div>

		<div style="height: 40px"></div>
	</table>
	</div>
</form>
</body>
</html>