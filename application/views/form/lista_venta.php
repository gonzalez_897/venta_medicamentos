<script type="text/javascript">
	$(document).ready(function () {
		$('.venta_lista').DataTable();
		$('.dataTables_length').addClass('bs-select');
	});
</script>
<body>
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
	<div align="center">
		<div>
			<h3 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Listado de ventas</h3>
			<div style="height: 50px"></div>
		</div>
		<div class="container">
			
			<?php if ($this->session->userdata('id_rol') === '1') {

				?>

				<div>
					<a class="btn" style="background-color: #26BFBF; color: white" href="<?php echo base_url(); ?>control_de_ventas/agregar_ventas"><font size="3">Agregar venta</font></a>
				</div>
			<?php } ?>
			<br>
			<br>
			<table align="center" class="table table-bordered table-hover" style="width: 1200px">
				
				<thead style="background-color: #26BFBF">
					<th class="text-center" style="color: white">idventa</th>
					<th class="text-center" style="color: white">fecha</th>
					<th class="text-center" style="color: white">factura</th>
					<th class="text-center" style="color: white">Ver todo</th>
				</thead>
				<tbody>
					<?php $numero = 1; ?>
					<?php foreach ($ventas as $valor): ?>
						<tr align="center">
							<td align="center" style="color:#687E8C"><?=$numero++; ?></td>
							<td align="center" style="color:#687E8C"><?=$valor->f_venta ?></td>
							<td align="center" style="color:#687E8C"><?=$valor->numero_facturacion ?></td> 
							<td>
								<a class="btn" style="background-color: #26BFBF; color: white" href="<?php echo base_url().'control_de_ventas/get_ventas/'.$valor->numero_facturacion; ?>"><font size="4"><i class="fas fa-bars"></i></font></a>
							</td>
						</tr>
					<?php endforeach;  ?>
				</tbody>
			</table>
		</div>

	</div>
</body>