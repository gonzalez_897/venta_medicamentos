<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-3.4.1.min.js' ?>"></script>
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
	<style type="text/css">
		.all{
			padding: 20px;
		}
		.all{

			width: 55%;
			padding: 35px;
			color: black;
			box-shadow: 10px 10px 8px 10px #888888;

		}

		.all .tama {
			border-radius: 10px; 
			width: 80%;
		}


		
		.all select {
			border-radius: 10px; 
			width: 80%;
		}

		.boton{
			width: 30%;
		}

		.nav{
			width: 90%;
		}

	</style>



	<script type="text/javascript">
		function Texto(string){
			var out = '';
			var filtro = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ ';

			for (var i=0; i<string.length; i++)
				if (filtro.indexOf(string.charAt(i)) != -1) 

					out += string.charAt(i);


				return out;
			} 



				
		</script>
	</head>
	<body>

		<div class="all container ">
			<div align="center">
        		<img style="height: 100px;width: 200px" src="<?php echo base_url('asset/imagen/logo.png');?>">
        	</div>

			<form action="<?php echo base_url(); ?>control_de_usuario/nuevo_usuario" method="post" autocomplete="off">
				<h3 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Registrar usuario</h3>
				<br>
				<label style="color: #95B3BF">Nombre:</label>
				<input type="text" id="nombre" class="form-control tama " onkeyup="this.value=Texto(this.value)"><p><?=form_error('usuario')?></p>
				<br>
				<label style="color: #95B3BF">Apellido:</label>
				<input type="text" id="apellido" class="form-control tama " onkeyup="this.value=Texto(this.value)">
				<br>
				<label style="color: #95B3BF">Edad</label>
				<input type="number" id="edad" class="form-control tama " min="17" max="80">
				<br>
				<label style="color: #95B3BF">Sexo:</label>
				<select id="sexo" class="form-control">
					<option value="">--Seleccione una opción--</option>
					<?php foreach ($sexo as $valor): ?>
						<option value="<?=$valor->id_sexo ?>"><?=$valor->sexo ?></option>
					<?php endforeach; ?>
				</select>
				<br>
				<label style="color: #95B3BF">E-mail:</label>
				<input type="text" id="correo" class="form-control tama " >
				<br>
				<label style="color: #95B3BF">Usuario:</label>
				<input type="text" id="usuario" class="form-control tama ">
				<br>
				<label style="color: #95B3BF">Contraseña:</label>
				<input type="password" id="contraseña" class="form-control tama " onkeyup="this.value=Texto(this.value)">
				<br>
				<label style="color: #95B3BF">Rol:</label>
				<select id="rol" class="form-control">
					<option value="">--Seleccione una opción--</option>
					<?php foreach ($rol as $valor): ?>
						<option value="<?=$valor->id_rol ?>"><?=$valor->rol ?></option>
					<?php endforeach; ?>
				</select>
				<br>
				<br>
				<br>
				
				<input style="background-color: #26BFBF; color: white" class="btn boton center" type="button" id="insert_user" value="Registrar datos">
				
			</form>
			
		</div> 
		<br>
		<br>
		<script type="text/javascript" src="<?php echo base_url().'asset/js/main.js' ?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'asset/js/insert_js/nuevo_user.js' ?>"></script>
	</body>
	</html>