
<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">

	<title>producto</title>
	<style type="text/css">

	</style>
</head>
<body>
	
	<div class="row" >
		<div class="container col-md-11">
			<br>
			<div style="margin-left: 100px">
				<?php if ($this->session->userdata('id_rol') === '1') {

					?>
					<button style="background-color:#26BFBF" class="btn"><a style="text-decoration: none; color:white" href="<?php echo base_url();?>control_de_categoria/agregar_dato_producto"><i class="fas fa-list"></i> Nuevo Producto</i> </a></button>
				<?php } ?>


				<?php if ($this->session->userdata('id_rol') === '2') {

					?>
					<button style="background-color:#26BFBF" class="btn"><a style="text-decoration: none; color:white" href="<?php echo base_url();?>control_de_categoria/agregar_dato_producto"><i class="fas fa-list"></i> Nuevo Producto</i> </a></button>
				<?php } ?>
			</div>
			<table align="center" class="table table-bordered" style="width: 1200px">
				<h3 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Registro de Productos</h3>
				<div style="height: 40px"></div>
				<tr>
					<thead style="background-color: #26BFBF">
						<th class="text-center" style="color: white">Numero De Barra</th>
						<th class="text-center" style="color: white">Producto</th>
						<th class="text-center" style="color: white">Fecha De Fabricación</th>
						<th class="text-center" style="color: white">Categoría</th>
						<th class="text-center" style="color: white">Descripción</th>
						<th class="text-center" style="color: white">Imagen</th>
						<th class="text-center" style="color: white">Stock</th>
						<th class="text-center" style="color: white">Cantidad</th>
						<th class="text-center" style="color: white">Fecha De Vencimiento</th>
						<th class="text-center" style="color: white">Proveedor</th>
						<th class="text-center" style="color: white">Precio</th>

						<?php if ($this->session->userdata('id_rol') === '1') {

							?>
							<th class="text-center" style="color: white">Opción</th>
							<th class="text-center" style="color: white">Opción</th>
						<?php } ?>
						<?php if ($this->session->userdata('id_rol') === '2') {

							?>
							
							<th class="text-center" style="color: white">Opción</th>
						<?php } ?>
					</thead>
				</tr>

				<tbody>
					<?php foreach ($producto as $P): ?>
						<tr>
							<td style="color:#687E8C" ><?=$P->numero_barra?></td>
							<td style="color:#687E8C" ><?=$P->nombre_producto?></td>
							<td style="color:#687E8C" ><?=$P->f_fabricacion?></td>
							<td style="color:#687E8C"><?=$P->categoria_producto?></td>
							<td style="color:#687E8C" ><?=$P->descripcion?></td>
							<td style="color:#687E8C" ><?=$P->imagen?></td>
							<td style="color:#687E8C" ><?=$P->stock?></td>
							<td style="color:#687E8C" ><?=$P->cantidad?></td>
							<td style="color:#687E8C" ><?=$P->fecha_vencimiento?></td>
							<td style="color:#687E8C" ><?=$P->primer_nombre?></td>
							<td style="color:#687E8C" ><?=$P->precio?></td>
							<?php if ($this->session->userdata('id_rol') === '1') {

								?>
								<td align="center"><a href="<?php echo base_url().'control_de_categoria/eliminar_producto/'.$P->numero_barra?>"><i class="fas fa-trash fa-lg" style="color:#26BFBF"></a></td>
									<td align="center"><a href="<?php echo base_url().'control_de_categoria/accion_producto/'.$P->numero_barra ?>"><i class="fas fa-edit fa-lg" style="color:#26BFBF"></a></td>
									<?php } ?>
									<?php if ($this->session->userdata('id_rol') === '2') {

										?>

										<td align="center"><a href="<?php echo base_url().'control_de_categoria/accion_producto/'.$P->numero_barra ?>"><i class="fas fa-edit fa-lg" style="color:#26BFBF"></a></td>
										<?php } ?>

									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<center>
							<p><a href="<?php echo base_url().'control_de_categoria/detalle_pdf/' ?>">Reporte PDF</a></p>
						</center>
					</div>
				</div>
			</body>
			</html>