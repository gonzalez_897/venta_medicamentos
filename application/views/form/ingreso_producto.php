<!DOCTYPE html>
<html>
<head>
	<title>Agregar Nuevos Productos</title>
	<script>
		function maxLengthCheck(object)
		{
			if (object.value.length > object.maxLength)
				object.value = object.value.slice(0, object.maxLength = 13)
		}

		function Texto(string){//Solo numeros
			var out = '';
    var filtro = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ1234567890. ';//Caracteres validos

    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i=0; i<string.length; i++)
    	if (filtro.indexOf(string.charAt(i)) != -1) 
             //Se añaden a la salida los caracteres validos
         out += string.charAt(i);

    //Retornar valor filtrado
    return out;
}  

function maxLengthCheck2(object)
{
	if (object.value.length > object.maxLength)
		object.value = object.value.slice(0, object.maxLength = 13)
}

		function Texto(string){//Solo numeros
			var out = '';
			var filtro = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ. ';

			for (var i=0; i<string.length; i++)
				if (filtro.indexOf(string.charAt(i)) != -1) 

					out += string.charAt(i);

				return out;
			}  

			function maxLengthCheck3(object)
			{
				if (object.value.length > object.maxLength)
					object.value = object.value.slice(0, object.maxLength = 3)
			}
		</script>

		<script type="text/javascript">
			setTimeout(function(){
				$('#alerta').fadeOut(500);
			},4000);
			setTimeout(function(){
				$('#alerta').fadeOut(500);
			},4000);
			
		</script>
		<style type="text/css">

			.all{
				padding: 20px;
			}
			.all{

				width: 75%;
				padding: 35px;
				color: black;
				box-shadow: 10px 10px 8px 10px #888888;

			}

			.all .tama {
				border-radius: 15px; 
				width: 75%;
			}



			.all select {
				border-radius: 15px; 
				width: 75%;
			}

			textarea{
				max-width: 590px;
				max-height: 100px;
				min-height: 50px;
			}
		</style>
	</head>
	<body>
		
		<div class="row">
			<div class="all container col-md-7">
				<div class="alert alert-success" id="alert" style="display: none;">&nbsp;</div>
				<h3> Nuevos Productos</h3>
				<br>
				<form name="agregar_producto" autocomplete="off" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>control_de_categoria/agregar_producto">

					<div class="form-group">
						<label for="name">Numero De Barra :&nbsp;&nbsp;</label><span id="alerta"></span>
						<input type="number" id="numero_barra" name="numero_barra"  class="form-control tama" oninput="maxLengthCheck(this)"  required>
					</div>
					<div class="form-group">
						<label>Nombre Del Producto :&nbsp;&nbsp;</label><span id="alerta1"></span>
						<input type="text" id="nombre_producto" name="nombre_producto" maxlength = "20"  class="form-control tama"  required>
					</div>
					<div class="form-group">
						<label>Fecha De Fabricación :</label><span id="alerta1"></span>
						<input type="date" id="f_fabricacion" class="form-control tama" min=<?php $hoy=date("Y-m-d"); echo $hoy;?> >
					</div>
					<div class="form-group">
						<label>Categoria :</label><span id="alerta1"></span>
						<select id="id_categoria" style="border-radius: 10px" class="form-control tama" required>
							<option>-- Seleccione La Categoria --</option>
							<?php foreach ($categoria as $valor): ?>
								<option value="<?=$valor->id_categoria?>"><?=$valor->categoria_producto ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Descripcion :</label><span id="alerta1"></span>
						<input type="text" id="descripcion" class="form-control tama" oninput="maxLengthCheck2(this)" maxlength = "100" onkeyup="this.value=Texto(this.value)" required>
					</div>
					<div class="form-group">
						<label>Imagen :</label><span id="alerta1"></span>
						<input type="text" id="imagen" class="form-control tama" required>
					</div>
					<div class="form-group">
						<label>Stock :</label><span id="alerta1"></span>
						<input type="number" id="stock" class="form-control tama" oninput="maxLengthCheck3(this)"  maxlength="2" required>
					</div>
					<div class="form-group">
						<label>Cantidad :</label><span id="alerta1"></span>
						<input type="number" id="cantidad" class="form-control tama" oninput="maxLengthCheck3(this)" required>
					</div>
					<div class="form-group">
						<label>Fecha De Vencimiento :</label><span id="alerta1"></span>
						<input type="date" id="fecha_vencimiento" class="form-control tama" required min=<?php $hoy=date("Y-m-d"); echo $hoy;?> required>
					</div>
					<div class="form-group">
						<label>Proveedor :</label><span id="alerta1"></span>
						<select id="id_proveedor" style="border-radius: 10px" class="form-control tama" required>
							<option>-- Seleccione El Proveedor</option>
							<?php foreach ($proveedor as $valor): ?>
								<option value="<?=$valor->id_proveedor?>"><?=$valor->primer_nombre?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Precio :</label><span id="alerta1"></span>
						<input type="number" id="precio" class="form-control tama" required>
					</div>
					<br>
					<div class="text-right">
						<button id="agregar_producto" class="btn btn-success">Guardar</button>
					</div>
				</form>
			</div>
		</div>
		
		<script type="text/javascript" src="<?php echo base_url();?>asset/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>asset/js/agregar_categoria.js"></script>
	</body>
	</html>