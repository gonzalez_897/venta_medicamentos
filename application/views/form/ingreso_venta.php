<head>
	<style type="text/css">
		.all{
			padding: 20px;
		}
		.all{

			width: 75%;
			padding: 35px;
			color: black;
			box-shadow: 10px 10px 8px 10px #888888;

		}

		.all .tama {
			border-radius: 15px; 
			width: 75%;
		}
	</style>
	<h1 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">INGRESE LA COMPRA</font></h1>
</head>

<body>
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
	<div>
		<div style="margin-top: 200px;">
			<div align="center" class="container">
				<div class="card fondo_venta imagen_border">
					<div>
						<img style="margin-left: 400px;margin-top: 10px" class="" width="150" height="75" src="<?php echo base_url(); ?>asset/imagen/logo.png">
					</div>
					<div class="formulario">
						<div class="titulo">

						</div>
						<div class=" imagen">
						</div>
						<div>
							<label><font size="4" style="color:#687E8C; position: fixed; top: 525px; left: 700px;">Productos:</font></label>
							<select style="margin-left: 200px;margin-top: 20px" id="producto" class="form-control col-sm-6">
								<option value="">---Seleccione un producto---</option>
								<?php foreach ($productos as $valor): ?>
									<?php $parametros = $valor->numero_barra.",".$valor->precio.",".$valor->nombre_producto.",".$valor->imagen; ?>
									<option value="<?=$parametros; ?>"><?=$valor->nombre_producto ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div>
							<input type="hidden" id="precio" >
							<input type="hidden" id="nombre" >
							<input type="hidden" id="codigo" >
							<input type="hidden" id="photos">
						</div>
						<div>
							<label><font size="4" style="color:#687E8C;position: fixed; top: 570px; left: 705px;">Cantidad:</font></label></div>
							<input type="number" class="form-control" id="cantidad" style="position: fixed; left: 840px; top: 565px; width: 250px;" min="0" max="<?=$valor->stock ?>">
						</div>
						<div>
							<button type="button" style="position: fixed; top: 600px; left: 850px;" class="btn btn-success" id="ingreso">Ingresar</button>
							<button style="position: fixed; top: 630px;" type="button" class="btn ver_ventas btn-primary" data-toggle="modal" data-target="#myModal">
								Ver ventas
							</button>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content" style="width: 800px;position: fixed; top: 150px; left: -129px;">

				<!-- Modal Header -->
				<div class="modal-header" style="background-color: #27697F">
					<img class="imagen_model" width="150" height="75" src="<?php echo base_url(); ?>asset/imagen/logo.png">
					<div  style="position: fixed;left: 200px;font-family: 'Archivo Black', sans-serif;color: white"><font size="4">LISTADO DE VENTAS</font></div>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<form action="<?php echo base_url(); ?>control_de_ventas/set_ventas" method="post">
						<div>
							<table class="table table-bordered table-hover" border="5" id="tabla">
								<thead style="background-color: #26BFBF">
									<tr class="text-center" style="color: white">
										<th>Codigo de barra</th>
										<th>productos</th>
										<th>precio</th>
										<th>cantidad</th>
										<th>total</th>
										<th>Eliminar</th>
									</tr>
								</thead>
								<tbody id="lista">
								</tbody>
								<tbody>
									<tr align="center">
										<td colspan="4" align="right"><strong>Total</strong></td>
										<td colspan="2">$<strong style="color:green;"><font id="resultado"></font></strong></td>
									</tr>
								</tbody>
							</table>
							<div>
								<input type="hidden" id="fecha" name="fecha" value="<?php echo date('Y-m-d'); ?>">
								<input type="hidden" id="factura" name="factura" value="<?php echo rand(1000,9999); ?>">
							</div>
						</div>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer" style="background-color: #D3D7D9">
						<button type="submit" class="btn btn-success">Enviar</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</body>