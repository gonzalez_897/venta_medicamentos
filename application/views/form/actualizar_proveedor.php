<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-3.4.1.min.js' ?>"></script>
    <link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
    <style type="text/css">
        .all{
            padding: 20px;
        }
        .all{

            width: 55%;
            padding: 35px;
            color: black;
            box-shadow: 10px 10px 8px 10px #888888;

        }

        .all .tama {
            border-radius: 10px; 
            width: 80%;
        }


        
        .all select {
            border-radius: 15px; 
            width: 80%;
        }

        .boton{
            width: 30%;
        }

        .nav{
            width: 90%;
        }

    </style>



    <script type="text/javascript">
        function Texto(string){
            var out = '';
            var filtro = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ ';

            for (var i=0; i<string.length; i++)
                if (filtro.indexOf(string.charAt(i)) != -1) 

                    out += string.charAt(i);


                return out;
            } 
        </script>
    </head>
    <body>

        <div class="all container ">
            <div>
                <img style="height: 100px;width: 200px" src="<?php echo base_url('asset/imagen/logo.png');?>">
            </div>

            <form action="<?php echo base_url(); ?>control_proveedores/actualizar_proveedores" method="post" autocomplete="off">
                <h3 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Actualización de datos</h3>
                <br>
                <input type="hidden" id="id_proveedor" value="<?php echo $proveedor->id_proveedor ?>">
                <label style="color: #95B3BF">Nombre</label>
                <input type="text" id="primer_nombre" class="form-control tama" value="<?php echo $proveedor->primer_nombre ?>" >
                <br>
                <label style="color: #95B3BF">Apellido</label>
                <input type="text" id="primer_apellido" class="form-control tama" value="<?php echo $proveedor->primer_apellido ?>">
                <br>
                <label style="color: #95B3BF">Correo</label>
                <input type="text" id="correo" class="form-control tama " max="80" value="<?php echo $proveedor->correo ?>">
                <br>
                <label style="color: #95B3BF">Edad</label>
                <input type="number" id="edad" class="form-control tama " value="<?php echo $proveedor->edad ?>" >
                <br>
                <label style="color: #95B3BF">Teléfono</label>
                <input type="number" id="telefono" class="form-control tama" value="<?php echo $proveedor->telefono ?>">
                <br>
                <label style="color: #95B3BF">Empresa</label>
                <input type="text" id="empresa" class="form-control tama " value="<?php echo $proveedor->empresa ?>">
                <br>
                <br>
                <br>
                
                <input style="background-color: #26BFBF; color: white" class="btn boton center" type="button" id="actualizar_proveedor" value="Actualizar">
                
            </form>
             
        </div>
        <br>
        <br>
        <script type="text/javascript" src="<?php echo base_url();?>asset/js/main.js"></script>
            <script type="text/javascript" src="<?php echo base_url();?>asset/js/actualizar_proveedor.js"></script>
    </div>
</body>

</html>



