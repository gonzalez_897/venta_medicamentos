<body>
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
	<div align="center">
		<div>
			<h3 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Listado de productos vendidos</h3>
			<div style="height: 50px"></div>
		</div>
		<div class="container">
			<table align="center" class="table table-bordered table-hover" style="width: 1200px">
				<thead style="background-color: #26BFBF">
					<tr>
					<th class="text-center" style="color: white">idventa</th>
					<th class="text-center" style="color: white">codigo de barra</th>
					<th class="text-center" style="color: white">Imagen</th>
					<th class="text-center" style="color: white">producto</th>
					<th class="text-center" style="color: white">precio</th>
					<th class="text-center" style="color: white">cantidad</th>
					<th class="text-center" style="color: white">total</th>
					<th class="text-center" style="color: white">fecha</th>
					<th class="text-center" style="color: white">factura</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($ventas as $valor): ?>
						<tr align="center" style="color:#687E8C">
							<td ><?=$valor->id_venta ?></td>
							<td><?=$valor->numero_barra ?></td>
							<td><img width="70" height="40" src="<?php echo base_url().'asset/imagen/'.$valor->imagen; ?>"></td>
							<td ><?=$valor->producto ?></td>
							<td >$<?=$valor->precio ?></td>
							<td ><?=$valor->cantidad ?></td>
							<td >$<?=$valor->total ?></td>
							<td ><?=$valor->f_venta ?></td>
							<td ><?=$valor->numero_facturacion ?></td> 
						</tr>
					<?php endforeach;  ?>
				</tbody>
			</table>
		</div>
		<div>
			<a class="btn" style="background-color: #26BFBF; color: white" href="<?php echo base_url(); ?>control_de_ventas/">Volver</a>
		</div>
	</div>
</body>