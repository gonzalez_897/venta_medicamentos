<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
	
	<div align="left">
		<div align="center" style="color:#26BFBF"><strong></strong></div>
	</div>
	<title></title>
	<script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-3.4.1.min.js' ?>"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	
	<style type="text/css">
		
		.fa-user-minus{
			font-size: 150%;
			
			color: black;
		}

		.fa-edit{
			font-size: 150%;
			color: black;
		}
		
		body{
			font-size:12px;
			font-family:Verdana;
		}
		



	</style>
	<script type="text/javascript">
		
	/*	$(document).ready(function() {
 
			$('.delete').click(function(e) {
				if(confirm('Seguro que quieres eliminar el usuario')){
					e.preventDefault();
					var parent = $(this).parent();
					var id = $('#id').val();
					$.ajax({

						type:'post',
						url:baseurl+'control_de_usuarios/delete_user',
 						data:{
							id
						},				
						success: function() {
							
							document.location.href=baseurl+'control_de_usuarios/';
						}
					});
				}
			});


		}); 






	</script>
</head>
<body style="background-color: #F2F2F2">
	<div class="container">
		<h3 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Registro de Usuarios</h3>
		<br>

		
		<button style="background-color: #26BFBF " class="btn"  ><a style="text-decoration: none; color:white;" href="<?php echo base_url(); ?>control_de_usuarios/usuarios">Nuevo usuario <i class="fas fa-user-plus"></i> </a></button>
		<br> 
		<br>
		<div style="height: 40px"></div>
	</table>
	<table table align="center" class="table table-bordered table-hover" style="width: 1200px">
		<thead style="background-color: #26BFBF">
			<tr>
				<th class="text-center" style="color: white" >Nombre</th>
				<th class="text-center" style="color: white">Apellido</th>
				<th class="text-center" style="color: white">Edad</th>
				<th class="text-center" style="color: white">Sexo</th>
				<th class="text-center" style="color: white">Correo electronico</th>
				<th class="text-center" style="color: white">Usuario</th>
				<?php if ($this->session->userdata('id_rol') === '1') {
					
					?>
					<th class="text-center" style="color: white">Rol</th>
					<th class="text-center" style="color: white">Opción</th>
					
				<?php } ?>
				<th class="text-center" style="color: white">Opción</th>
			</tr>
		</thead>
		<?php foreach ($usuario as $u):?>

			<tr>
				<tbody >
					<td align="center" style="color:#687E8C"><?=$u->nombre  ?></td>
					<td align="center" style="color: #687E8C"><?=$u->apellido  ?></td>
					<td align="center" style="color: #687E8C"><?=$u->edad  ?></td>
					<td align="center" style="color: #687E8C"><?=$u->sexo  ?></td>
					<td align="center" style="color: #687E8C"><?=$u->correo_electronico  ?></td>
					<td align="center" style="color: #687E8C"><?=$u->usuario  ?></td>

					<?php  if ($this->session->userdata('id_rol') === '1') { ?>
						<td align="center" style="color: #687E8C"><?=$u->rol  ?></td>
						<td align="center" >
							<div id="record-1" class="record">
								<a onclick="return confirm('Quieres eliminar el usuario?');" style="text-decoration: none;color" href="<?php echo base_url().'control_de_usuarios/delete_user/'.$u->id_usuario ?>"><i style="color: #26BFBF" class="fas fa-user-minus"></i></a>
							</div>
						</td>
					<!-- <div id="record-1" class="record">
							<a class="delete" id="<?=$u->id_usuario ?>" style="text-decoration: none;color" href="<?php echo base_url().'control_de_usuarios/delete_user/'.$u->id_usuario ?>"><i style="color: #26BFBF" class="fas fa-user-minus"></i></a>
						</div>
						

						<td align="center" >
						<div id="record-1" class="record">
							<a class="delete" id="" style="text-decoration: none;color" href="?delete=<?php echo base_url().'control_de_usuarios/delete_user/'.$u->id_usuario ?>"><i style="color: #26BFBF" class="fas fa-user-minus"></i></a>
						</div>
					</td>
					<td align="center"><a style="text-decoration: none;color" href="<?php echo base_url().'control_de_usuarios/delete_user/'.$u->id_usuario ?>"><i style="color: #26BFBF" class="fas fa-user-minus"></i></a></td> -->
					
				<?php }  ?>
				<td align="center"><a style="text-decoration: none;" href="<?php echo base_url().'control_de_usuarios/update_user/'.$u->id_usuario ?>"><i style="color: #26BFBF" class="far fa-edit"></i></a></td>
			</tbody> 


		<?php endforeach; ?>
	</table>
</div>




<script type="text/javascript" src="<?php echo base_url();?>asset/js/main.js"></script>


</body>
</html>