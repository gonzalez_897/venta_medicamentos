<!DOCTYPE html>
<html>
<head>
	<title></title>

	
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

	<style type="text/css">

		.all{
			padding: 20px;
		}
		.all{

			width: 75%;
			padding: 35px;
			color: black;
			box-shadow: 10px 10px 8px 10px #888888;

		}

		.all .tama {
			border-radius: 15px; 
			width: 75%;
		}



		.all select {
			border-radius: 15px; 
			width: 75%;
		}
	</style>
</head>
<body> 


	<br>
	<br>
	<div class="row">
		<div class="all container col-md-7">
			<h3 class="text-dark">Ingrese Categoria</h3>
			<br>

			<form name="agregar_categoria" autocomplete="off" method="post">
				<!--<div class="alert alert-danger" role="alert"><?php //echo validation_errors(); ?></div>
				<?php //echo form_open('form')?> -->
				<div>
					<div class="form-group">
						
						<label for="categoria_producto">Nombre de la categoria:&nbsp; &nbsp; </label><span id="alerta"></span>
						<br>	


						<input type="text" id="categoria_producto" class="form-control" name="categoria_producto" maxlength="20" size="20">

							
						
					</div>
				</div>
				<div><input type="button" id="agregar_categoria" value="agregar" class="btn btn-info"></div>
				
				<br>
			</form>

		</div>
	</div>



	<script type="text/javascript" src="<?php echo base_url();?>asset/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/js/agregar_categoria.js"></script>
	<script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-3.4.1.min.js' ?>"></script>
	<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>