<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-3.4.1.min.js' ?>"></script>
    <link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
    <style type="text/css">
        .all{
            padding: 20px;
        }
        .all{

            width: 55%;
            padding: 35px;
            color: black;
            box-shadow: 10px 10px 8px 10px #888888;

        }

        .all .tama {
            border-radius: 10px; 
            width: 80%;
        }


        
        .all select {
            border-radius: 15px; 
            width: 80%;
        }

        .boton{
            width: 30%;
        }

        .nav{
            width: 90%;
        }

    </style>



    <script type="text/javascript">
        function Texto(string){
            var out = '';
            var filtro = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ ';

            for (var i=0; i<string.length; i++)
                if (filtro.indexOf(string.charAt(i)) != -1) 

                    out += string.charAt(i);


                return out;
            } 
        </script>
    </head>
    <body>

        <div class="all container ">
        	<div align="center">
        		<img style="height: 100px;width: 200px" src="<?php echo base_url('asset/imagen/logo.png');?>">
        	</div>
        	<div style="height: 30px"></div>
            <form >
                <h3 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Registro de proveedores</h3>
                <br>
                <label style="color: #95B3BF">Nombre</label>
                <input type="text" id="primer_nombre" class="form-control tama " >
                <br>
                <label style="color: #95B3BF">Apellido</label>
                <input type="text" id="primer_apellido" class="form-control tama ">
                <br>
                <label style="color: #95B3BF">Correo</label>
                <input type="text" id="correo" class="form-control tama "max="80">
                <br>
                <label style="color: #95B3BF">Edad</label>
                <input type="number" id="edad" class="form-control tama " >
                <br>
                <label style="color: #95B3BF">Teléfono</label>
                <input type="number" id="telefono" class="form-control tama ">
                <br>
                <label style="color: #95B3BF">Empresa</label>
                <input type="text" id="empresa" class="form-control tama ">
                <br>
                <br>
                <br>
                
                <input style="background-color: #26BFBF; color: white" class="btn boton center" type="button" id="agregar_proveedor" value="Registrar">
                
            </form>
            
        </div>
        <br>
        <br>
        <script type="text/javascript" src="<?php echo base_url();?>asset/js/main.js"></script>
			<script type="text/javascript" src="<?php echo base_url();?>asset/js/agregar_proveedor.js"></script>
    </div>
</body>

</html>


