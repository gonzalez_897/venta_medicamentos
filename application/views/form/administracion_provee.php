<!DOCTYPE html>
<html>
<head>
  <title>Proveedores</title>
</head>
<div align="left">
<img style="height: 100px; width: 200px" src="<?php echo base_url('asset/imagen/logo.png') ?>">
<div align="center" style="color:#26BFBF"><strong>Proveedores</strong></div>
</div>
<body>
  <table class="table table-bordered table-hover">
    <thead style="background-color: #26BFBF" >
</div>
    
      <th style="color: white">Nombre</th>
      <th  style="color: white">Apellido</th>
      <th style="color: white">Correo</th>
      <th style="color: white">Edad</th>
      <th style="color: white">Teléfono</th>
      <th style="color: white">Empresa</th>
      <th style="color: white">Eliminar</th>
      <th style="color: white">Actualizar</th>
      
      
    </thead>
    <?php   foreach ($proveedor as $p){?>
      <tr>
        <td style="color:#023059"><?=$p->primer_nombre?></td>
        <td style="color:#023059"><?=$p->primer_apellido?></td>
        <td style="color:#023059"><?=$p->correo?></td>
        <td style="color:#023059"><?=$p->edad?></td>
        <td style="color:#023059"><?=$p->telefono?></td>
        <td style="color: #023059"><?=$p->empresa?></td>
        <td><a href="<?php echo base_url('control_proveedores/eliminar_proveedores/').$p->id_proveedor ?>"><i class="fas fa-trash fa-lg" style="color:#26BFBF"></i></a></td>
        <td><a href="<?php echo base_url('control_proveedores/actualizar_proveedor/').$p->id_proveedor ?>"><i class="fas fa-edit fa-lg" style="color:#26BFBF"></i></a></td>
      </tr>
    <?php } ?>
    <br><br>
  
    
    <a href="<?php echo base_url('control_proveedores/agregar_proveedor/')?>"><i class="btn fas fa-plus-square fa-lg" style="width: 100px; background-color:#26BFBF;color: white;border-radius: 12px "></i></a>
  </table>
    </form>
</body>
</html>