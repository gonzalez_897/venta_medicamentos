<!DOCTYPE html>
<html>    
<head>
  <title>Inicio</title>
  <script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-3.4.1.min.js' ?>"></script>
  <script type="text/javascript">

   /* if(window.history.forward(1) != null) window.history.forward(1); */
    
  </script> 
</head>
<div style="height: 30px"></div>
<body style="background-size: cover, fixed, center"  background="<?php echo base_url('asset/imagen/fondo.png');?>">
  <!-- notificacion -->
  <!-- fin notificacion -->
  <div class="opacity container h-100" style="width: 400px; border-radius: 25px;height: 800px"> 
    <div class="d-flex justify-content-center h-100">
      <div class="user_card">
        <div class="d-flex justify-content-center">
          <div class="brand_logo_container">
            <img style="height:100px; width: 200px" src="<?php echo base_url('asset/imagen/logo.png');?>">
          </div>
           
        </div>
        <div style="height:30px"></div>
        <div class="d-flex justify-content-center form_container">
          <form class="user" action="<?php echo base_url();?>login_controller/auth" method="POST" autocomplete='off'>
            <?php echo $this->session->flashdata('msg');  ?>
            <div class="input-group mb-3">
              <div class="input-group-append">
                <span class="input-group-text"><i style="color:#26BFBF" class="fas fa-user style"></i></span>
              
              <input type="text" name="usuario" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Usuario" autofocus required>
            </div>
            <div style="height: 20px"></div>
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i style="color: #26BFBF" class="fas fa-key"></i></span>
            
              <input type="password" name="contraseña" class="form-control form-control-user"  placeholder="Contraseña" required>
            </div>
          </div> 
          <div style="height: 20px"></div>       
          <div class="d-flex justify-content-center mt-3 login_container">
            <button type="submit" name="login" class="btn btn-block" style="background-color: #26BFBF; color: white;margin-top: -16px; width: 247px;">Login</button>
          </div>

          <div class="text-center" style="margin-top: 17px; margin-bottom: 10px;" data-toggle="modal" data-target="#basicExampleModal">
            <a href="<?php echo base_url();?>login_controller/forget" style="color:#26BFBF;text-decoration: none;">¿Olvidaste tu contraseña?</a>
          </div>

        </form>
      </div>
    </div>

  
 
</body>
</html>
</html>