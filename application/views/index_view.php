<!DOCTYPE html>
<html>
<head>
  <title>Pharmacy Store</title>
<style type="text/css">
  hr{
    background: aqua;
    height: 1px;
  }
  .flip-card {
  background-color: transparent;
  width: 300px;
  height: 200px;
  border:2px color:gray; ;
  perspective: 1000px; /* Remove this if you don't want the 3D effect */
}

/* This container is needed to position the front and back side */
.flip-card-inner {
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 0.8s;
  transform-style: preserve-3d;

}

/* Do an horizontal flip when you move the mouse over the flip box container */
.flip-card:hover .flip-card-inner {
  transform: rotateY(180deg);
}

/* Position the front and back side */
.flip-card-front, .flip-card-back {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
}

/* Style the front side (fallback if image is missing) */
.flip-card-front {
  background-color: 
  color: black;
}

/* Style the back side */
.flip-card-back {
  background-color:#F8F9FD;
  color: white;
  transform: rotateY(180deg);
}

</style>

</head>
<div >
</div>
<body>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url('asset/imagen/farmacia1.png') ?>">
    </div>

    <div class="item">
      <img src="<?php echo base_url('asset/imagen/farmacia2.png') ?>">
    </div>

    <div class="item">
      <img src="<?php echo base_url('asset/imagen/farmacia3.png') ?>">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div style="background-color: #26BFBF;color: white;">
  <h1 style="margin-top: 20px" align="center">CATEGORÍAS</h1>
</div>
<div style="height: 30px"></div>
<?php foreach ($categoria as $C): ?>
  <div class="col-md-3 p-3">
<div class="flip-card">
  <div class="flip-card-inner">
    <div class="flip-card-front">
      <img src="<?php echo base_url().'asset/imagen/'.$C->imagen; ?>" alt="Avatar" style="width:200px;height:200px;">
    </div>
    <div class="flip-card-back">
      <a href="<?php echo base_url(); ?>control_de_ventas/agregar_ventas">
        <p style="font-size: 30pt;color: #27697F;margin-top: 60px"><?=$C->categoria_producto?></p>
      </a>
    </div>
  </div>
</div>
</div>
<?php endforeach; ?>
<div style="height: 60px"></div>


  </div>
</div>
</div>
<?php require "footer.php" ?>
</body>
</html>