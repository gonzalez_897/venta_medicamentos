<!DOCTYPE html>
<html>
<head>
	<title>categoria</title>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

	<script>
		function myFunction() {
			var input, filter, ul, li, a, d;
			input = document.getElementById("mySearch");
			filter = input.value.toUpperCase();
			ul = document.getElementById("myMenu");
			li = ul.getElementsByTagName("li");
			for (d = 0; d < li.length; d++) {
				a = li[d].getElementsByTagName("a")[0];
				if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
					li[d].style.display = "";
				} else {
					li[d].style.display = "none";
				}
			}
		}

		function Texto(string){//Solo numeros
			var out = '';
    var filtro = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ';//Caracteres validos

    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i=0; i<string.length; i++)
    	if (filtro.indexOf(string.charAt(i)) != -1) 
             //Se añaden a la salida los caracteres validos
         out += string.charAt(i);

    //Retornar valor filtrado
    return out;
}  
</script>

<style type="text/css">
	table{
		position: relative;
		top:50px;
		left:0px;
		border-radius: ;
		font-weight: normal;
		padding: 3px 5px;
		padding-top: 50px;
		padding-right: 30px;
		padding-bottom: 50px;
		padding-left: 80px;
		width: 270px;
		height: 100%;
		font-size: 15pt;
		text-align: center;
		padding: 20px 0 0;
		margin: -1em 0 0;
		height: 90px;


	}
	
	
	.topnav {
		position: relative;
		overflow: hidden;
		background-color: #34383F;
		height: 66px;
		top: 30px;
	}


	.topnav .search-container {
		float: right;
	}

	.topnav input[type=text] {
		padding: 8px;
		margin-top: 8px;
		margin-left: -12px;

		font-size: 17px;
		border: none;
	}

	.topnav .search-container button {
		float: right;
		padding: 6px 10px;
		margin-top: 8px;
		margin-right: 16px;
		background: #ddd;
		font-size: 17px;
		border: none;
		cursor: pointer;
	}

	
	@media screen and (max-width: 600px) {
		.topnav .search-container {
			float: none;
		}
		.topnav a, .topnav input[type=text], .topnav .search-container button {
			float: none;
			display: block;
			text-align: left;
			width: 100%;
			margin: 0;
			padding: 14px;
		}
		.topnav input[type=text] {
			border: 1px solid #ccc;  
		}
	}

	.row {
		display: fle;
	}

	/* Left column (menu) */
	.left {
		flex: 35%;
		padding: 15px 0;
	}

	.left h2 {
		padding-left: 8px;
	}

	/* Right column (page content) */
	.right {
		flex: 65%;
		padding: 15px;
	}

	/* Style the search box */
	#mySearch {
		width: 100%;
		font-size: 18px;
		padding: 11px;
		border: 1px solid #ddd;
	}

	/* Style the navigation menu inside the left column */
	#myMenu {
		list-style-type: none;
		padding: 0;
		margin: 0;
	}

</style>
</head>
<body>
	
	<div class="container ">
		<h3>Categoria de productos</h3>
		<br>
		
		<button class="btn btn-success"><a style="color:white; text-decoration: none;" href="<?php echo base_url();?>control_de_categoria/agregar_dato">Agregar Datos</a></button>
		
		<div class="topnav">
			<div class="search-container">
				<form action="/action_page.php">
					<div class="input-group">

						<input type="text" id="mySearch" onkeyup="myFunction();this.value=Texto(this.value)" placeholder="Buscar Categoria" />
					</div>
				</form>
			</div>
		</div>
		<ul id="myMenu">

			<table class="table table-dark table-hover">
				<tr>
					<thead>

						<td>Categoria de Productos</td>
						<td>Opción</td>
						<td>Opción</td>
					</thead>
				</tr>


				<tbody>
					<?php foreach ($categoria as $C): ?>
						<tr>

							<td><li><a><?=$C->categoria_producto?></a></li></td>
							<td><li><a href="<?php echo base_url().'control_de_categoria/eliminar/'.$C->id_categoria?>"><img src="<?php echo base_url();?>asset/imagen/trash.png" style="height: 45px; width: 45px"></a></li></td>

							<td><li><a href="<?php echo base_url().'control_de_categoria/accion/' .$C->id_categoria?>"><img src="<?php echo base_url();?>asset/imagen/refresh.png" style="height: 45px; width: 45px"></a></li></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</ul>
	</div>

</div>  
</body>
</html>