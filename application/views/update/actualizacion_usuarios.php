<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-3.4.1.min.js' ?>"></script>
	<style type="text/css">
		.all{
			padding: 20px;
		}
		.all{

			width: 55%;
			padding: 35px;
			color: black;
			box-shadow: 10px 10px 8px 10px #888888;

		}

		.all .tama {
			border-radius: 10px; 
			width: 80%;
		}



		.all select {
			border-radius: 10px; 
			width: 80%;
		}

		.boton{
			width: 30%;
		}

	</style>
	</head>
	<body>
		<div class="all container">
			<div align="center">
				<img style="height: 100px;width: 200px" src="<?php echo base_url('asset/imagen/logo.png');?>">
			</div>

			<form action="<?php echo base_url(); ?>control_de_usuarios/actualizar_usuarios" method="post" autocomplete="off">
				<h3 align="center" style="font-family: 'Archivo Black', sans-serif;color: #687E8C">Actualizar Datos</h3><i class="icon-loop21"></i></h3>
				<br>
				<label style="color: #95B3BF">Nombre:</label>
				<input type="hidden" id="idusuario" value="<?=$user->id_usuario ?>">
				<input type="text" id="nombre" value="<?=$user->nombre ?>" class="form-control tama " onkeyup="this.value=Texto(this.value)">
				<br>
				<label style="color: #95B3BF">Apellido:</label>
				<input type="text" id="apellido" value="<?=$user->apellido ?>" class="form-control tama " onkeyup="this.value=Texto(this.value)">
				<br>
				<label style="color: #95B3BF">Edad</label>
				<input type="number" id="edad" value="<?=$user->edad ?>" class="form-control tama " min="17" max="80" onkeyup="this.value=Texto(this.value)">
				<br>
				<label style="color: #95B3BF">Sexo:</label>
				<select id="sexo" class="form-control">
					<option value="">--Seleccione una opción--</option>
					<?php foreach ($sexo as $valor): ?>
						<?php if ($valor->id_sexo == $user->id_sexo): ?>
							<option value="<?=$valor->id_sexo ?>" selected><?=$valor->sexo ?></option>
							<?php else: ?>
								<option value="<?=$valor->id_sexo ?>"><?=$valor->sexo ?></option>
							<?php endif ?>
						<?php endforeach; ?>
					</select>
					<br>
					<label style="color: #95B3BF">E-mail:</label>
					<input type="text" id="correo" value="<?=$user->correo_electronico ?>" class="form-control tama " onkeyup="this.value=Texto(this.value)">
					<br>
					<label style="color: #95B3BF">Usuario:</label>
					<input type="text" value="<?=$user->usuario ?>" id="usuario" class="form-control tama " onkeyup="this.value=Texto(this.value)">
					<br>
					<label style="color: #95B3BF">Contraseña:</label>
					<input type="password" id="contraseña" value="<?=$user->contraseña ?>" class="form-control tama " onkeyup="this.value=Texto(this.value)">
					<br>
					<br>
					<br>
					<br>

					<input style="background-color: #26BFBF; color: white" class="btn boton" type="button" id="update_user" value="Guardar datos">

				</form>

			</div>
			<br>
			<br>
			<script type="text/javascript" src="<?php echo base_url().'asset/js/main.js' ?>"></script>
			<script type="text/javascript" src="<?php echo base_url().'asset/js/update_js/actualizacion_usuario.js' ?>"></script>
		</body>
		</html>