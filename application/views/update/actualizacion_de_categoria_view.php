<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-3.4.1.min.js';?>"></script>
	<style type="text/css">

		.all{
			padding: 20px;
		}
		.all{

			width: 75%;
			padding: 35px;
			color: black;
			box-shadow: 10px 10px 8px 10px #888888;

		}

		.all .tama {
			border-radius: 15px; 
			width: 75%;
		}



		.all select {
			border-radius: 15px; 
			width: 75%;
		}
	</style>
</head>
<body>
	<br>
	<br>
	<form name="actualizacion_de_categoria" autocomplete="off" action="<?php echo base_url();?>control_de_categoria/editar" method="post">
		<div class="row">
			<div class="all container col-md-7">
				<h3>Actualización de información</h3>
				<br>
				<div class="form-group">
					<label>Nombre de la catgoria:</label>
					<br>
					<input class="form-control" type="text" id="categoria_producto" value="<?=$categoria->categoria_producto?>">
					<input type="hidden" id="id_categoria" value="<?=$categoria->id_categoria?>">
					<br>
	
					<input class="btn btn-danger" type="button" id="actualizacion" value="Actualizar datos">
				</div>
			</div>
		</div>
	</form>
	<script type="text/javascript" src="<?php echo base_url();?>asset/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/js/actualizacion_de_categoria.js"></script>
</body>
</html>