<!DOCTYPE html>
<html>
<head>
	<title>actualizar producto</title>
	<script type="text/javascript" src="<?php echo base_url().'asset/js/jquery-3.4.1.min.js';?>"></script>
	<style type="text/css">

		.all{
			padding: 20px;
		}
		.all{

			width: 75%;
			padding: 35px;
			color: black;
			box-shadow: 10px 10px 8px 10px #888888;

		}

		.all .tama {
			border-radius: 15px; 
			width: 75%;
		}



		.all select {
			border-radius: 15px; 
			width: 75%;
		}

		textarea{
			max-width: 590px;
			max-height: 100px;
			min-height: 50px;
		}
	</style>
</head>
<body>
	<form name="actualizar_producto" autocomplete="off" action="<?php echo base_url();?>control_de_categoria/editar_producto" method="post">
		<div class="row">
			<div class="all container col-md-7">
				<h3>Actualizacion De Productos</h3>
				<br>
				<div>
					<label>Nombre Del Producto</label>
					<input type="text" id="nombre_producto" value="<?=$producto->nombre_producto?>" class="form-control tama">
				</div>
				<div>
					<label>Fecha De Fabricacion</label>
					<input type="date" id="f_fabricacion" value="<?=$producto->f_fabricacion?>" class="form-control tama">
				</div>

				<div>
					<label>Categoria</label>
					<select id="id_categoria" class="form-control select" class="form-control tama">
						<option>-- Actualice La Categoria --</option>
						<?php foreach ($categoria as $categoria_producto):
							if($categoria_producto->id_categoria==$producto->id_categoria) {?>
								<option value="<?=$categoria_producto->id_categoria;?>" selected><?=$categoria_producto->categoria_producto?></option>
							<?php }else{ ?>
								<option value="<?=$categoria_producto->id_categoria;?>"><?=$categoria_producto->categoria_producto ?></option>
							<?php } endforeach ?>
						</select>
					</div>
					<div>
						<label>Descripcion</label>
						<input type="text" id="descripcion" value="<?=$producto->descripcion?>" class="form-control tama"> 
					</div>
					<div>
						<label>Imagen</label>
						<input type="text" id="imagen" value="<?=$producto->imagen?>" class="form-control tama">
					</div>
					<div>
						<label>Stock</label>
						<input type="text" id="stock" value="<?=$producto->stock?>" class="form-control tama">
					</div>
					<div>
						<label>Cantidad</label>
						<input type="number" id="cantidad" value="<?=$producto->cantidad?>" class="form-control tama">
					</div>
					<div>
						<label>Fecha de vencimiento</label>
						<input type="date" id="fecha_vencimiento" value="<?=$producto->fecha_vencimiento?>"  class="form-control tama">
					</div>
					<div>
						<label>Proveedor</label>
						<select id="id_proveedor"  class="form-control tama">
							<option>-- Actualice El Proveedor --</option>
							<?php foreach ($proveedor as $primer_nombre):
								if($primer_nombre->id_proveedor==$primer_nombre->id_proveedor) {?>
									<option value="<?=$primer_nombre->id_proveedor;?>" selected><?=$primer_nombre->primer_nombre?></option>
								<?php }else{ ?>
									<option value="<?=$primer_nombre->id_proveedor;?>"><?=$primer_nombre->primer_nombre ?></option>
								<?php } endforeach ?>
							</select>
						</div>
						<div>
							<label>Precio</label>
							<input type="number" id="precio" value="<?=$producto->precio?>"  class="form-control tama">
						</div>
						<div>
							<input type="hidden" id="numero_barra" value="<?=$producto->numero_barra?>"  class="form-control tama"><br>
							<input class="btn btn-danger" type="button" id="actualizar_producto" value="Actualizar datos">
						</div>
					</div>	
				</div>
			</form>
			<script type="text/javascript" src="<?php echo base_url();?>asset/js/main.js"></script>
			<script type="text/javascript" src="<?php echo base_url();?>asset/js/actualizacion_de_categoria.js"></script>
		</body>
		</html>