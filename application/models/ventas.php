<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');




class ventas extends CI_model
{
	public function get_ventas()
	{
		$this->db->distinct('numero_facturacion');
		$this->db->select('v.f_venta,v.numero_facturacion');
		$this->db->from('venta as v');
		$ventas = $this->db->get();
		return $ventas->result();
	}

	public function productos()
	{
		$producto = $this->db->get('producto');
		return $producto->result();
	}

	public function get_productos($data)
	{
		$this->db->select('precio');
		$this->db->from('producto');
		$this->db->where('numero_barra',$data['precio']);
		$precio = $this->db->get();
		return $precio->result();
	}

	public function mostrar_producto($dato)
	{
		$this->db->select('v.id_venta,p.numero_barra,v.producto,p.imagen,v.precio,v.cantidad,v.total,v.f_venta,v.numero_facturacion');
		$this->db->from('venta as v');
		$this->db->join('producto as p','p.numero_barra = v.numero_barra');
		$this->db->where('numero_facturacion',$dato['factura']);
		$ventas = $this->db->get();
		return $ventas->result();
	}

	public function set_ventas($data)
	{
		return ($this->db->insert("venta",$data)) ? true:false;
	}
}


 ?>