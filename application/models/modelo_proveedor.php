<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class modelo_proveedor extends CI_model
{
	
	public function obtener_proveedor(){
		$proveedor= $this->db->get('proveedor');
		return $proveedor->Result();
	}

	public function act_proveedor($id){
		$this->db->where('id_proveedor', $id);
		$proveedor= $this->db->get('proveedor');
		return $proveedor->row();
	}

	

	public function update($data){

		$this->db->where('id_proveedor',$data['id_proveedor']);
		return ($this->db->update('proveedor', $data)) ? true:false;

		}

		public function get_proveedor($id){
		$this->db->where('id_proveedor = ', $id);
		$proveedor = $this->db->get('proveedor');
		return $proveedor->row();
	}

	
	public function insert($data){

		
		return($this->db->insert('proveedor', $data)) ? true:false;

		
	}

	

	public function delete_prove($id){

		$this->db->where('id_proveedor',$id);
		return($this->db->delete('proveedor')) ? true:false;
	}

	

}

?>