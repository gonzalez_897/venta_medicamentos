<?php 
defined('BASEPATH')OR exit('No direct script access allowed');

class categoria_model extends CI_model{

	public function categoria(){

		$categoria = $this->db->get('categoria');
		return $categoria->result();

	}

	public function eliminar($id){
		$this->db->where('id_categoria', $id);
		return($this->db->delete('categoria'))? true:false;
	}


	public function insert($data){
		return($this->db->insert('categoria', $data)) ? true:false;
	}

	public function obtener($id){
		$this->db->select('id_categoria,categoria_producto');
		$this->db->from('categoria');
		$this->db->where('id_categoria ='.$id);
		$categoria = $this->db->get();
		return ($categoria->num_rows() === 1)? $categoria->row(): false;
	}

	public function actualizar($data){
		$this->db->where('id_categoria', $data['id_categoria']);
		return($this->db->update('categoria', $data))? true:false;
	}

	/////////////// PRODUCTO ///////////////////


	public function producto(){
		$this->db->select('p.numero_barra, p.nombre_producto, p.f_fabricacion, c.categoria_producto, p.descripcion, p.imagen, p.stock, p.cantidad, p.fecha_vencimiento, v.primer_nombre, p.precio');
		$this->db->from('producto p');
		$this->db->join('categoria c', 'c.id_categoria = p.id_categoria');
		$this->db->join('proveedor v', 'v.id_proveedor = p.id_proveedor');
		$producto = $this->db->get();
		return $producto->result();
	}

	public function get_producto()
	{
		$producto = $this->db->get('producto');
		return $producto->result();
	
	}

	public function eliminar_producto($id){
		$this->db->where('numero_barra', $id);
		return($this->db->delete('producto'))? true:false;
	}
	public function proveedor(){
		$proveedor = $this->db->get('proveedor');
		return $proveedor->result();
	}
	
	public function insertar_producto($data){
		return($this->db->insert('producto', $data))? true:false;
	}

	public function obtener_producto($numero_barra){
		$this->db->select('numero_barra,nombre_producto,f_fabricacion,id_categoria,descripcion,imagen, stock, cantidad, fecha_vencimiento, id_proveedor, precio');
		$this->db->from('producto');
		$this->db->where('numero_barra ='.$numero_barra);
		$producto = $this->db->get();
		return ($producto->num_rows() === 1)? $producto->row(): false;
	}

	public function actualizar_producto($data){
		$this->db->where('numero_barra', $data['numero_barra']);
		return($this->db->update('producto',$data))? true:false;
	}
}

?>
