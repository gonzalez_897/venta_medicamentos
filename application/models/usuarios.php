<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Model {


	public function get_usuarios($value='')
	{
		$this->db->select('*');
		$this->db->from('usuario');
		$this->db->join('sexo','usuario.id_sexo = sexo.id_sexo');
		$this->db->join('rol','usuario.id_rol = rol.id_rol');
		$usuario = $this->db->get();

		return $usuario->result();
	}

	public function sexo()
	{
		$sexo = $this->db->get('sexo');

		return $sexo->result();
	}


	public function rol()
	{
		$rol = $this->db->get('rol');

		return $rol->result();
	}


	public function all_user($id)
	{
		$this->db->select('nombre,apellido,edad,id_sexo,correo_electronico,usuario,contraseña,id_usuario');
		$this->db->from('usuario');
		$this->db->where('id_usuario ='.$id);
		$user = $this->db->get();


		return ($user->num_rows() === 1) ? $user->row(): false;
	}

	public function insert_user($data)
	{
		return ($this->db->insert('usuario',$data)) ? true:false;
	}

	public function update_user($data)
	{
		$this->db->where('id_usuario',$data['id_usuario']);
		return ($this->db->update('usuario',$data)) ? true:false;
	}


	public function del_user($id)
	{		
		$datos = $this->db->get('usuario');
		if ($numero = $datos->num_rows() > 4) {
			$this->db->select('id_usuario');
			$this->db->where('id_usuario ='. $id);
			$this->db->where('id_usuario !='.$this->session->userdata('id_usuario'));
			
			return ($this->db->delete('usuario')) ? true:false;

		}
	}

	public function get_usu()
	{
		$usu = $this->db->get('usuario');
		return $usu->result();

		
		
	}

} 
